This is the Java server for the android app Mbuddies , 
this server project is part of the final work for the cycle of programming in Java.
The server uses hibernate technology with mySql connection. 

Technologies :

	JEE
	Gson
	Hibernate
	MySQL
	Websockets
	Servlets 3.1

The server was deployed to OpenShift for Mbuddies app project presentation, but is no longer online.
You can see the project documentation in the docs folder.

To download the Mbuddies source code, please visit :
https://gitlab.com/Amado/Mbuddies/tree/master
