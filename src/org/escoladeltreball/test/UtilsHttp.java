package org.escoladeltreball.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import org.escoladeltreball.exception.ValidationException;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class UtilsHttp {

	public static Class<?> deSerializeFromJson(String jsonObject, Class<?> clase) {
		Class<?> paquete = null;
		try {

			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.setPrettyPrinting().serializeNulls().create();

			paquete = (Class<?>) gson.fromJson(jsonObject, clase);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paquete;
	}

	public static String createUrl2(final String action, final String[] params,
			final String[] values) throws UnsupportedEncodingException,
			ValidationException {

		String result = null;
		try {
			if (params == null || values == null) {
				result = TestConfig.SERVER_URL + action;
			} else {
				if (params.length != values.length) {
					throw new ValidationException(
							"Exception creating url the number of params and values must be the same");
				}
				StringBuilder paramsValues = new StringBuilder("?");
				for (int i = 0; i < values.length; i++) {
					paramsValues.append(params[i]
							+ "="
							+ URLEncoder.encode(values[i],
									TestConfig.Connection.CHARACTER_ENCODING)
							+ "&");
				}
 
			}
			int a = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("result = " + result);
		}

		return result;
	}

	/**
	 * @deprecated
	 * @param action
	 * @param params
	 * @param values
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ValidationException
	 */
	public static String createUrl(final String action, final String[] params,
			final String[] values) throws UnsupportedEncodingException,
			ValidationException {

		String result = null;
		try {
			if (params == null || values == null) {
				result = TestConfig.SERVER_URL + action;
			} else {
				if (params.length != values.length) {
					throw new ValidationException(
							"Exception creating url the number of params and values must be the same");
				}
				StringBuilder paramsValues = new StringBuilder("?");
				for (int i = 0; i < values.length; i++) {
					paramsValues.append(URLEncoder.encode(params[i],
							TestConfig.Connection.CHARACTER_ENCODING)
							+ "="
							+ URLEncoder.encode(values[i],
									TestConfig.Connection.CHARACTER_ENCODING)
							+ "&");
 
				}

				paramsValues = paramsValues.delete(paramsValues.length() - 1,
						paramsValues.length());

				result = TestConfig.SERVER_URL + action
						+ paramsValues.toString();
				System.out.println("before encode");
				System.out.println("result = " + result);

			}
			int a = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("result = " + result);
		}
 

		return result;
	}

	public static String performPostCall(String requestURL,
			Map<String, String> postDataParams) {
		// final int readTimeout = 15000;
		// final int connectTimeout = 15000;
		System.out.println("performPostCall...");
		URL url;
		String response = "";
		try {
			url = new URL(requestURL);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(TestConfig.Connection.readTimeout);
			conn.setConnectTimeout(TestConfig.Connection.connectTimeout);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			if (postDataParams.isEmpty() == false) {
				OutputStream os = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os,
								TestConfig.Connection.CHARACTER_ENCODING));
				final String postdata = getPostData(postDataParams);
				System.out.println("performPostCall: " + requestURL + "?"
						+ postdata);
				writer.write(getPostData(postDataParams));
				writer.flush();
				writer.close();
				os.close();

			}

			int responseCode = conn.getResponseCode();

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				while ((line = br.readLine()) != null) {
					response += line;
				}
			} else {
				response = "";

				throw new Exception(responseCode + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	private static String getPostData(Map<String, String> params)
			throws UnsupportedEncodingException {
		StringBuilder paramsStr = new StringBuilder();
		Set<Entry<String, String>> paramsSet = params.entrySet();
		for (Map.Entry<String, String> entry : paramsSet) {
			paramsStr.append(URLEncoder.encode(entry.getKey(),
					TestConfig.Connection.CHARACTER_ENCODING));
			paramsStr.append("=");
			paramsStr.append(URLEncoder.encode(entry.getValue(),
					TestConfig.Connection.CHARACTER_ENCODING));
			paramsStr.append("&");
		}

		if (paramsStr.length() != 0) {
			paramsStr.delete(paramsStr.length() - 1, paramsStr.length());
		}

		return paramsStr.toString();
	}
}
