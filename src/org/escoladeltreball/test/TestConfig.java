package org.escoladeltreball.test;

public interface TestConfig {

	public static final boolean TEST_APP = true;
	public static final String SHARED_PREF_NAME = "SHARED_PREF_NAME";
	public static final String SHARED_PREF_SESSION = "SHARED_PREF_SESSION";
	public static final String JSON_OB_TYPE_ERROR = "ERROR";
	public static final String JSON_OB_TYPE_SUCCESS = "SUCCESS";

	public static final String SERVER_URL = "http://127.0.0.1:8080/WebServer/HttpServlet/";
	 

	//
	public static final String URL_WEBSOCKET = "ws://192.168.2.32:8080/WebServer/chat?name=";

	public static final String TAG_INFO = "INFO", TAG_ERROR = "ERROR";
	public static final String TAG_WARNING = "WARNING";

	public static interface Connection {
		public static final String CHARACTER_ENCODING = "UTF-8";
		public static final int readTimeout = 15000, connectTimeout = 15000;
	}

	public static interface Validation {
		public static final int USER_NAME_MIN_LENGTH = 3,
				USER_NAME_MAX_LENGTH = 12;
		public static final int PASS_MIN_LENGTH = 6, PASS_MAX_LENGTH = 20;
	}

	/*
	 * <SHARED VARIBLES BEETWEN SERVER AND CLIENT FOR COMMUNICATION>
	 */
	public static interface HttpArgs {
		/*
		 * Users attribures
		 */
		// public static final String latitude = "latitude";
		// public static final String longitude = "longitude";
		public static final String idUser = "idUser";
		public static final String username = "username";
		// public static final String mail = "mail";
		public static final String password = "password";
		public static final String city = "city";
		public static final String country = "country";
		public static final String personalDescription = "personalDescription";
		public static final String musicPreferences = "musicPreferences";
		public static final String photo = "country";
		public static final String photoResource = "photoResource";

		/*
		 * favorites to identify a song y server db: the identify field instead
		 * of idFavorite
		 */
		// public static final String idFavorites = "idFavorites";
		public static final String favoritesName = "favoritesName";
		public static final String favoritesNewName = "favoritesNewName";
		public static final String favoritesIdentification = "favoritesIdentification";
		public static final String favoriteAndItsSongs = "favoriteAndItsSongs";

		/*
		 * songs attributes to identify a song y server db: is necessary the
		 * userId and the songSrcPath
		 */
		// public static final String idSong = "idSong";
		public static final String songName = "songName";
		public static final String songAuthor = "songAuthor";
		public static final String songSrcPath = "songSrcPath";
		/*
		 * users_friends
		 */
		public static final String idUserFriend = "idUserFriend";
		public static final String idUserOwner = "idUserOwner";
	}

	public static interface HttpActions {
		/*
		 * Users
		 */
		public static final String LOGIN = "login";  
		public static final String SIGNUP = "signUp"; 
		public static final String UPDATE_USER_PROFILE = "updateProfile"; 
		public static final String REMOVE_USER = "removeUser"; 
		//
		public static final String FIND_ALL_USERS = "findAllUsers"; 
		public static final String FIND_USER = "findUser"; 
		public static final String FIND_USERS_BY_COUNTRY_AND_CITY = "findUsersByCountryAndCity"; 
		public static final String UPDATE_CURRENT_PLAY_SONG = "findCurrentPlaySong"; 
		public static final String FIND_DISTINCT_COUNTRIES_AND_CITIES = "findDistinctCountriesAndCities"; 
		//
		public static final String TEST = "test"; 
		public static final String CREATE_TEST_DATA = "createTestData";  

		/*
		 * Friends (idenficiation of users by name instead of their id)
		 */
		public static final String SEND_FRIEND_REQUEST = "sendFriendRequest";
		public static final String RESPONSE_FRIEND_REQUEST = "responseFriendRequest";
		public static final String REMOVE_FRIEND = "removeFriend";
		public static final String ADD_FRIEND = "addFriend";
		public static final String FIND_FRIENDS_FROM_USER = "findFriendsFromUsers";
		/*
		 * Songs / Favorites
		 */
		public static final String UPDATE_SONG = "updateSong"; 
		public static final String ADD_SONG_TO_FAVORITE = "addSongToFavorite";  
		public static final String REMOVE_SONG_FROM_FAVORITE = "removeSongFromFavorite";
		// ADD_OR_UPDATE_FAVORITE -> (only updates its name)
		public static final String ADD_OR_UPDATE_FAVORITE = "addUpdateFavorite";  
		public static final String UPDATE_FAVORITE_AND_ITS_SONGS = "updateFavoriteAndItsSongs";
		// REMOVE_FAVORITE -> (only removes the orphans songs)
		public static final String REMOVE_FAVORITE = "removeFavorite"; 

		// FIND_SONGS -> params: [ user | favorite ]
		public static final String FIND_SONGS = "findSongs"; 
		public static final String FIND_FAVORITES_FROM_USER = "findFavoritesFromUser";  

	}
	/*
	 * </SHARED VARIBLES BEETWEN SERVER AND CLIENT FOR COMMUNICATION>
	 */

}