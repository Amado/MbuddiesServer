package org.escoladeltreball.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.escoladeltreball.config.Config;
import org.escoladeltreball.model.json.FavoriteAndSongs;
import org.escoladeltreball.model.json.ListFavorites;
import org.escoladeltreball.model.json.ListSongs;
import org.escoladeltreball.model.json.ListUsers;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.persistence.Canciones;
import org.escoladeltreball.model.persistence.Favorites;
import org.escoladeltreball.model.persistence.Users;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * class to test the client - server operations
 * 
 */
public class TestClient implements TestConfig {

	

	public static void test() {
		/**
		 * <pre>
		 * IMPORTANTE:
		 * antes de realizar alguna operacion de consulta/modificacion/creacion de
		 * 	 canciones o lista de favoritos
		 * del propioUsuario/cliente en el servidor se debería hacer un update
		 * 	 desde el cliente con los datos
		 * de los favoritos al servidor para que la informacion este sincronizada.
		 * > ejecutar updateAllFavorites UPDATE_FAVORITE_AND_ITS_SONGS para cada
		 * 	 favorito que se haya modificado
		 * mientras el dispositivo haya estado desconectado.
		 * </pre>
		 */

		/**
		 * users
		 * 
		 */
		/**
		 * <pre>
		 * LOGIN / SIGNUP
		 * (el login y signup devuelve el usuario con sus datos y el _id_
		 * asignado en la DB del servidor, que se usara para otras operaciones)
		 * </pre>
		 */
		// System.out.println("TEST SIGNUP................");
		// testSignUp("Michael122", "123456");

		// System.out.println("TEST LOGIN................");
		// testLogin("Michael122", "123456");

		// System.out.println("TEST LOGIN................");
		// testRemoveUser("Michael122");

		// testRemoveUser("Carlos");

		/**
		 * FIND USERS
		 */
		// System.out.println("FIND ALL USERS ................");
		// testFindallUsers();

		// System.out.println("FIND USER ................");
		// testFindUser("23");
		//
		// System.out.println("FIND USER BY CITY AND COUNTRY................");
		// testFindUsersByCountryAndCity("Spain", "Barcelona");

		// System.out.println("FIND USER ONLY BY COUNTRY................");
		// testFindUsersByCountryAndCity("Spain", null);
		//
		// System.out.println("FIND USER ONLY BY CITY ................");
		// testFindUsersByCountryAndCity(null, "Barcelona");

		/**
		 * UPDATE USERS
		 */
		// System.out.println("UDPDATE USER ................");
		// testUpdateUser("20");

		/*
		 * UPDATE CURRENT PLAY SONG
		 */
		// System.out.println("UDPDATE CURRENT PLAY SONG ................");
		// testUpdateUpdateCurrentPlaySong(
		//
		// "/ANDROID.RESOURCE://ORG.ESCOLADELTREBALL/MUSIC/SULANSOFSWING_DIRESTRAITS.MP3",
		// "21");
		// testUpdateUpdateCurrentPlaySong(
		//
		// "/android.resource://org.escoladeltreball/music/i_don't_want_miss_a_thing.mp3",
		// "21");

		/**
		 * users / friends (chat)
		 * 
		 * el caso de uso: addFriendrequest (ha de ser testeado sobre la app
		 * porque requiere una comunication entre clientes y servidor)
		 */

		/**
		 * ADD/REMOVE FRIENDS
		 */
		// System.out.println("ADD FRIEND ................");
		// testAddFriend("20", "21");
		// testAddFriend("22", "21");
		//
		// System.out.println("REMOVE FRIEND ................");
		// testRemoveFriend("20", "21");
		// testRemoveFriend("22", "21");

		/**
		 * FIND FRIENDS
		 */
		// System.out.println("FIND FRIENDS ................");
		// testFindFriendFromUser("21");

		/**
		 * songs / favorites
		 * 
		 * <pre>
		 * ********************
		 * ** IMPORTANTE **
		 * ********************************************************************
		 * Para actualizar/eliminar una lista de favoritos
		 * no le podemos pasar el id de la lista (Se ha de pasar, en los favoritos
		 * 	 el campo identification)
		 * en el caso de las canciones pasa lo mismo, se identifica por: idUsuario
		 * 	 y srcPath (la ruta del recurso);
		 * 
		 * </pre>
		 */
		/**
		 * FIND FAVORITES
		 */
		// System.out.println("FIND FAVORITES FROM USER ................");
		// testFindFavoritesFromUser("20");
		//

		/**
		 * ADD FAVORITES
		 */

		//
		// System.out
		// .println("ADD FAVORITES NEW FAVORITES PASSING AN IDENTIFICATION  (1) ................");
		// el identificador a de ser único -> idUser:[ long | String ]
		// testAddFavorites(("21:" + System.currentTimeMillis()), "newList...",
		// "21");
		// System.out
		//
		// .println("ADD FAVORITES NEW FAVORITES PASSING AN IDENTIFICATION   (2) ................");
		// testAddFavorites(("21:" + 123456790), "Favorites 8", "21");
		// System.out
		//
		// .println("UPDATE FAVORITES BY FAVORITES.IDENTIFICATION ................");
		// testUpdateFavoritesName("21:" + 123456789, "21", "Favorites 8.1");
		//

		//
		/**
		 * UPDATE SONG
		 */
		// srcPath, idUser, songName, songAuthor
		//
		// System.out.println("UPDATE SONG ................ ");
		// testUpdateSong(
		//
		// "/android.resource://org.escoladeltreball/music/i_don't_want_miss_a_thing.mp3",
		// "21", "I DON'T WANNA MISS A THING", "AEROSMITH");
		//
		// // System.out.println("UPDATE SONG ................ ");
		// testUpdateSong(
		//
		// "/android.resource://org.escoladeltreball/music/i_don't_want_miss_a_thing.mp3",
		// "21", "I don't wanna miss a thing", "Aerosmith");
		//

		/**
		 * FIND SONG
		 */
		// System.out.println("FIND SONGS BY ID_USER ................");
		// testFindSongs("20", null);

		// System.out.println("FIND SONGS BY ID_FAVORITES ................");
		// testFindSongs(null, "20:423421");

		/**
		 * ADD SONG TO FAVORITES LIST
		 */
		// System.out.println("ADD SONG TO FAVORITES LIST ................");
		// no se puede agregar la misma cancion a una lista de favoritos
		// aunque el servidor hace la comprobación (y si no la hiciese daría un
		// error y la operación no se efectuaría),
		// --_> __Es el cliente quien debe comprobar que se esta agregando una
		// cancion
		// repetida a una misma lista__
		//
		// testAddSongToFavorites(
		// "21:" + 123456790,
		// "FAVORITES 8.2",
		// "21",
		//
		// "/ANDROID.RESOURCE://ORG.ESCOLADELTREBALL/MUSIC/SULANSOFSWING_DIRESTRAITS.MP3",
		// "SULTANS OF SWING", "DIRE Straits");
		//
		// testAddSongToFavorites(
		// "21:" + 123456789,
		// "Favorites 8.1",
		// "21",
		//
		// "/android.resource://org.escoladeltreball/music/sulansofswing_direstraits.mp3",
		// "Sultans of Swing", "Dire Straits");

		// testAddSongToFavorites(
		// "21:" + 123456790,
		// "Favorites 8.12",
		// "21",
		//
		// "/android.resource://org.escoladeltreball/music/bohemian_rhapsody.mp3",
		// "Bohemian Rhapsody", "Queen");
		//
		//
		// testAddSongToFavorites(
		// "21:" + 123456789,
		// "Favorites 8.2",
		// "21",
		//
		// "/android.resource://org.escoladeltreball/music/bohemian_rhapsody.mp3",
		// "Bohemian Rhapsody", "Queen");
		//
		//
		// System.out.println("REMOVE SONG FROM FAVORITES LIST ................");
		// testRemoveSongFromFavorites(
		// "21:" + 123456789,
		// "Favorites 8.3",
		//
		// "/android.resource://org.escoladeltreball/music/sulansofswing_direstraits.mp3",
		// "21");

		/**
		 * REMOVE SONG
		 */

		// testRemoveSongFromFavorites(
		// "21:" + 123456789,
		// "Favorites 8.66",
		//
		// "/android.resource://org.escoladeltreball/music/sulansofswing_direstraits.mp3",
		// "21");
		//
		// testRemoveSongFromFavorites(
		// "21:" + 123456790,
		// "Favorites 8.4",
		//
		// "/android.resource://org.escoladeltreball/music/sulansofswing_direstraits.mp3",
		// "21");

		/**
		 * REMOVE FAVORITES
		 */
		// System.out.println("REMOVE FAVORITE ................");
		// testRemoveFavorite("21:" + 123456790);
		// testRemoveFavorite("21:" + 123456789);

		/**
		 * UPDATE FAVORITE AND ITS SONGS
		 */
		// System.out.println("UPDATE FAVORITE AND ITS SONGS ................");
		// testupdateFavoritesAndItsSongs(); // test: updates 21:112345678
	}

	/**
	 * <pre>
	 * ***********************************************************************
	 * 
	 * IMPORTANTE
	 * 
	 * ***********************************************************************
	 * si queremos uno o mas parámetros sean null, por algun motivo,
	 * 	 simplemente
	 * no lo pasamos; ya que _No podemos usar null como valor para un
	 * 	 parámetro_
	 * (para actualizar la currentPlaySong existe un metodo específico ->
	 * 	 'updateCurrentPlaySong' )
	 * </pre>
	 */
	public static void testUpdateUser(final String id) {
		// final String id = "1";
		final String username = "Carla 2";
		final String password = "newPassword2";
		final String city = "newCity2";
		final String country = "newCountry1100";
		final String personalDescription = "a personal description 2";
		final String musicPreferences = "rock, pop, jazz11 2";
		final String photo = "2";
		final String photoResource = "android.resource://org.app/img.2";

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUser, id);
					paramsMap.put(HttpArgs.username, username);
					paramsMap.put(HttpArgs.password, password);
					paramsMap.put(HttpArgs.city, city);
					paramsMap.put(HttpArgs.country, country);
					paramsMap.put(HttpArgs.personalDescription,
							personalDescription);
					paramsMap.put(HttpArgs.musicPreferences, musicPreferences);
					paramsMap.put(HttpArgs.photo, photo);
					paramsMap.put(HttpArgs.photoResource, photoResource);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.UPDATE_USER_PROFILE,
							paramsMap);

					System.out
							.println("UPDATE_USER_PROFILE() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Users user = gson.fromJson(responseString.toString(),
							new Users().getClass());

					System.out
							.println("UPDATE_USER_PROFILE() - user fetched from server = "
									+ user);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testLogin(final String username, final String password) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.username, username);
					paramsMap.put(HttpArgs.password, password);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL + Config.HttpActions.LOGIN,
							paramsMap);

					System.out.println("login() response String from server : "
							+ responseString);

					Gson gson = new GsonBuilder().create();

					final Users user = gson.fromJson(responseString.toString(),
							new Users().getClass());

					System.out.println("login() - user fetched from server = "
							+ user);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testRemoveUser(final String userName) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					paramsMap.put(HttpArgs.username, userName);

					String responseString = UtilsHttp
							.performPostCall(TestConfig.SERVER_URL
									+ Config.HttpActions.REMOVE_USER, paramsMap);

					System.out
							.println("testRemoveUser() response String from server : "
									+ responseString);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testSignUp(final String username, final String password) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.username, username);
					paramsMap.put(HttpArgs.password, password);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL + Config.HttpActions.SIGNUP,
							paramsMap);

					System.out
							.println("testSignUp() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Users user = gson.fromJson(responseString.toString(),
							new Users().getClass());

					System.out
							.println("testSignUp() - user fetched from server = "
									+ user);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	/**
	 * podemos buscar usuarios por ciudad y pais o sólo por uno de los dos
	 * campos
	 * 
	 * @param country
	 * @param city
	 */
	public static void testFindUsersByCountryAndCity(final String country,
			final String city) {
		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					if (country != null) {
						paramsMap.put(HttpArgs.country, country);
					}

					if (city != null) {
						paramsMap.put(HttpArgs.city, city);
					}

					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.FIND_USERS_BY_COUNTRY_AND_CITY,
									paramsMap);

					System.out
							.println("testFindUsersByCountryAndCity() ... retrieving the data");

					System.out
							.println("testFindUsersByCountryAndCity() response String from server : "
									+ responseString);

					Object responseObject = (Object) parseResponseFromServer(
							responseString, new ListUsers());

					/**
					 * parseamos la respuesta del servidor y en funcion de si el
					 * resultado es una lista de usuarios/objeto serializable o
					 * un [mensaje de error/exito] -> StatusPackage (en funcion
					 * de la peticion realizada), luego realizamos las
					 * operaciones correspondientes
					 */
					if (responseObject instanceof ListUsers) {
						ListUsers listUsers = (ListUsers) responseObject;
						if (responseObject != null && listUsers.list != null) {

							for (Users iterable_element : listUsers.list) {
								System.out.println(String.format(
										"user.nombre = %s",
										iterable_element.getName()));
							}
						}
						if (listUsers.list.isEmpty() == false) {
							System.out
									.println(listUsers.list.get(0).toString());
						} else {
							System.out
									.println("no se ha encontrano ningún usuario con los parametros");
						}

					} else {
						StatusPackage statusPackage = (StatusPackage) responseObject;
						System.out
								.println("mensaje de éxito ó error   del servidor ? "
										+ statusPackage.getType());
						System.out
								.println("mensaje de estado o causa del error del servidor: "
										+ statusPackage.getMessage());
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testFindallUsers() {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					// paramsMap.put(HttpArgs.idUser, id);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.FIND_ALL_USERS,
							paramsMap);

					System.out
							.println("testFindallUserS() ... retrieving the data");

					System.out
							.println("testFindallUserS() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					ListUsers listUsers = gson.fromJson(
							responseString.toString(),
							new ListUsers().getClass());

					if (listUsers != null && listUsers.list != null) {
						for (Users iterable_element : listUsers.list) {
							System.out.println(String.format(
									"user.nombre = %s",
									iterable_element.getName()));

						}
					}

					List<Users> users = listUsers.list;

					System.out.println(users.get(0).toString());

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testFindUser(final String id) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUser, id);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.FIND_USER, paramsMap);

					System.out
							.println("FindUser() ... retrieving the data from user : "
									+ id);

					System.out
							.println("findUser() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Users user = gson.fromJson(responseString.toString(),
							new Users().getClass());

					System.out
							.println("findUser() - user fetched from server = "
									+ user);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testFindFriendFromUser(final String idUser) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUser, idUser);

					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.FIND_FRIENDS_FROM_USER,
									paramsMap);

					System.out
							.println("FindUser() ... retrieving the data from user : "
									+ idUser);

					System.out
							.println("findUser() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					ListUsers listUsers = gson.fromJson(responseString,
							new ListUsers().getClass());

					if (listUsers != null && listUsers.list != null) {
						for (Users iterable_element : listUsers.list) {
							System.out.println(String.format(
									"user.nombre = %s",
									iterable_element.getName()));

						}
					}

					List<Users> users = listUsers.list;

					if (users != null) {
						for (Users u : users) {
							System.out.println(u.toString());
						}
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testFindSongs(final String idUser,
			final String favoritesIdentification) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					if (favoritesIdentification != null) {
						paramsMap.put(HttpArgs.favoritesIdentification,
								favoritesIdentification);
					}
					if (idUser != null) {
						paramsMap.put(HttpArgs.idUser, idUser);
					}
					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.FIND_SONGS, paramsMap);

					System.out
							.println("FindSongsByUser() ... retrieving the data from user : "
									+ idUser);

					System.out
							.println("FindSongsByUser() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					ListSongs listSongs = gson.fromJson(responseString,
							new ListSongs().getClass());

					if (listSongs != null && listSongs.list != null) {
						System.out
								.println("FindSongsByUser() - song fetched from server = "
										+ listSongs.list.size());
					}

					List<Canciones> songs = listSongs.list;
					if (songs != null && songs.isEmpty() == false) {
						System.out.println("songs = " + songs);

						System.out.println(songs.get(0));
						System.out.println(songs.get(0).getSongName());
						Canciones a = convertInstanceOfObject(songs.get(0),
								Canciones.class);
						a.getIdSong();

						System.out.println("songs:..............");
						for (Canciones song : listSongs.list) {
							System.out.println(song.getIdSong() + ": "
									+ song.getSongName() + " ->"
									+ song.toString());
						}
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testAddFriend(final String idUserSelf,
			final String idUserFriend) {
		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUserOwner, idUserSelf);
					paramsMap.put(HttpArgs.idUserFriend, idUserFriend);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.ADD_FRIEND, paramsMap);

					System.out
							.println("testAddFriend() ... retrieving the data from user : "
									+ idUserSelf);

					System.out
							.println("testAddFriend() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Users song = gson.fromJson(responseString.toString(),
							new Users().getClass());

					System.out
							.println("testAddFriend() - friend fetched from server = "
									+ song);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testRemoveFriend(final String idUserSelf,
			final String idUserFriend) {
		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUserOwner, idUserSelf);
					paramsMap.put(HttpArgs.idUserFriend, idUserFriend);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.REMOVE_FRIEND,
							paramsMap);

					System.out
							.println("testAddFriend() ... retrieving the data from user : "
									+ idUserSelf);

					System.out
							.println("testAddFriend() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Users song = gson.fromJson(responseString.toString(),
							new Users().getClass());

					System.out
							.println("testAddFriend() - friend fetched from server = "
									+ song);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testUpdateSong(final String srcPath,
			final String idUser, final String songName, final String songAuthor) {
		// final String id = "1";
		// final String songName = "Yellow33";
		// final String songAuthor = "ColdPlay";

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.songSrcPath, srcPath);
					paramsMap.put(HttpArgs.idUser, idUser);
					paramsMap.put(HttpArgs.songName, songName);
					paramsMap.put(HttpArgs.songAuthor, songAuthor);

					String responseString = UtilsHttp
							.performPostCall(TestConfig.SERVER_URL
									+ Config.HttpActions.UPDATE_SONG, paramsMap);

					System.out
							.println("testUpdateSong() ... retrieving the data from user : "
									+ idUser);

					System.out
							.println("testUpdateSong() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Canciones song = gson.fromJson(
							responseString.toString(),
							new Canciones().getClass());

					System.out
							.println("testUpdateSong() - song fetched from server = "
									+ song);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testFindFavoritesFromUser(final String idUser) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUser, idUser);

					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.FIND_FAVORITES_FROM_USER,
									paramsMap);

					System.out
							.println("testFavoritesFromUser() ... retrieving the favorites from user : "
									+ idUser);

					System.out
							.println("testFavoritesFromUser() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final ListFavorites listFavorites = gson.fromJson(
							responseString.toString(),
							new ListFavorites().getClass());

					System.out
							.println("testFavoritesFromUser() - Favorites fetched from server = "
									+ listFavorites);

					if (listFavorites.list != null) {
						for (Favorites favorite : listFavorites.list) {
							System.out.println("favorite: " + favorite);
						}
					} else {
						System.out.println("The user " + idUser
								+ " doesn't have any favorite yet!");
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testAddFavorites(final String favoritesIdentification,
			final String favoritesNewName, final String idUser) {

		new Thread(new Runnable() {
			public void run() {
				/*
				 * IMPORTANTE: favoritesIdentification sirve como identificador
				 * por lo que no se puede repetir nunca formato:
				 * idusuario:xxxxxxxxxxxxxxxxxxx
				 * 
				 * eg: "21:" + System.currentTimeMillis()
				 */
				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUser, idUser);
					paramsMap.put(HttpArgs.favoritesNewName, favoritesNewName);
					paramsMap.put(HttpArgs.favoritesIdentification,
							favoritesIdentification);
					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.ADD_OR_UPDATE_FAVORITE,
									paramsMap);

					System.out
							.println("testAddFavorites() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final ListFavorites listFavorites = gson.fromJson(
							responseString.toString(),
							new ListFavorites().getClass());

					System.out
							.println("testAddFavorites() - Favorites fetched from server = "
									+ listFavorites);

					StatusPackage statusPackage = (StatusPackage) parseResponseFromServer(
							responseString, null);

					if (statusPackage != null) {
						System.out
								.println("mensaje de éxito ó error   del servidor ? "
										+ statusPackage.getType());
						System.out
								.println("mensaje de estado o causa del error del servidor: "
										+ statusPackage.getMessage());
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testUpdateFavoritesName(final String favoritesName,
			final String idUser, final String newFavoritesName) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.idUser, idUser);
					paramsMap.put(HttpArgs.favoritesIdentification,
							favoritesName);
					paramsMap.put(HttpArgs.favoritesNewName, newFavoritesName);

					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.ADD_OR_UPDATE_FAVORITE,
									paramsMap);

					System.out
							.println("testAddFavorites() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final ListFavorites listFavorites = gson.fromJson(
							responseString.toString(),
							new ListFavorites().getClass());

					System.out
							.println("testAddFavorites() - Favorites fetched from server = "
									+ listFavorites);

					StatusPackage statusPackage = (StatusPackage) parseResponseFromServer(
							responseString, null);

					if (statusPackage != null) {
						System.out
								.println("mensaje de éxito ó error   del servidor ? "
										+ statusPackage.getType());
						System.out
								.println("mensaje de estado o causa del error del servidor: "
										+ statusPackage.getMessage());
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	/**
	 * para actualizar la cancion que se esta escuchando le tenemos que pasar el
	 * idUser y el path del resource que identifica la cancion (ya que el id de
	 * una cancion en la base de datos remota no sera el mismo que el que tenga
	 * en local, ya que si no no se podrían crean canciones desde el dispositivo
	 * local)
	 * 
	 * @param srcSongPath
	 * @param idUser
	 */
	public static void testUpdateUpdateCurrentPlaySong(
			final String srcSongPath, final String idUser) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					// la src de la nueva cancion
					paramsMap.put(HttpArgs.songSrcPath, srcSongPath);
					paramsMap.put(HttpArgs.idUser, idUser);

					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.UPDATE_CURRENT_PLAY_SONG,
									paramsMap);
					System.out
							.println("testUpdateUpdateCurrentPlaySong() response String from server : "
									+ responseString);

					Gson gson = new GsonBuilder().create();

					final Canciones song = gson.fromJson(
							responseString.toString(),
							new Canciones().getClass());

					System.out
							.println("testUpdateUpdateCurrentPlaySong() - song fetched from server = "
									+ song);

					StatusPackage statusPackage = (StatusPackage) parseResponseFromServer(
							responseString, null);
					if (statusPackage != null) {
						System.out
								.println("mensaje de éxito ó error   del servidor ? "
										+ statusPackage.getType());
						System.out
								.println("mensaje de estado o causa del error del servidor: "
										+ statusPackage.getMessage());
					}

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	/*
	 * .getParameter(HttpArgs.favoritesIdentification); String srcPath =
	 * request.getParameter(HttpArgs.songSrcPath); String songName =
	 * request.getParameter(HttpArgs.songName); String songAuthor =
	 * request.getParameter(HttpArgs.songAuthor);
	 */
	public static void testAddSongToFavorites(

	final String favoritesIdentification, final String favoritesName,
			final String idUser, final String songSrcPath,
			final String songName, final String songAuthor) {

		// new Thread(new Runnable() {
		// public void run() {

		try {

			Map<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put(HttpArgs.favoritesIdentification,
					favoritesIdentification);
			paramsMap.put(HttpArgs.idUser, idUser);
			paramsMap.put(HttpArgs.favoritesName, favoritesName);
			paramsMap.put(HttpArgs.songSrcPath, songSrcPath);
			paramsMap.put(HttpArgs.songName, songName);
			paramsMap.put(HttpArgs.songAuthor, songAuthor);

			String responseString = UtilsHttp.performPostCall(
					TestConfig.SERVER_URL
							+ Config.HttpActions.ADD_SONG_TO_FAVORITE,
					paramsMap);

			System.out
					.println("testUpdateSong() response String from server : "
							+ responseString);

			Gson gson = new GsonBuilder().create();

			final Canciones song = gson.fromJson(responseString.toString(),
					new Canciones().getClass());

			System.out
					.println("testAddSongToFavorites() - song fetched from server = "
							+ song);

		} catch (Throwable e) {
			System.out.println("error" + e.toString());
			e.printStackTrace();
		} finally {
		}

		// }
		// }).start();

	}

	public static void testRemoveSongFromFavorites(
			final String favoritesIdentification, final String favoritesName,
			final String songSrcPath, final String idUser) {
		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					paramsMap.put(HttpArgs.favoritesIdentification,
							favoritesIdentification);
					paramsMap.put(HttpArgs.favoritesName, favoritesName);
					paramsMap.put(HttpArgs.songSrcPath, songSrcPath);
					paramsMap.put(HttpArgs.idUser, idUser);
					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.REMOVE_SONG_FROM_FAVORITE,
									paramsMap);

					System.out
							.println("testRemoveSongFromFavorites() response String from server : "
									+ responseString);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();

	}

	public static void testRemoveFavorite(final String favoritesIdentification) {

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					paramsMap.put(HttpArgs.favoritesIdentification,
							favoritesIdentification);

					String responseString = UtilsHttp.performPostCall(
							TestConfig.SERVER_URL
									+ Config.HttpActions.REMOVE_FAVORITE,
							paramsMap);

					System.out
							.println("testRemoveSongFromFavorites() response String from server : "
									+ responseString);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	public static void testupdateFavoritesAndItsSongs() {

		new Thread(new Runnable() {
			public void run() {

				/**
				 * <pre>
				 *  creamos una lista de favoritos con todos los datos de la
				 *  lista, sus canciones; el usuario lo especificamos directamente sobre el
				 * 	 objeto
				 *  FavoriteAndSongs
				 * 
				 * ******************************************************************************
				 *  IMPORTANTE:
				 * 
				 *  usar el identificador de la lista, en este caso:
				 *  '21:112345678'
				 * 
				 ******************************************************************************* 
				 * </pre>
				 */

				final Long userId = 21L;
				Favorites favorite = new Favorites("21:112345678",
						"Favorites 21");
				HashSet<Canciones> songs = new HashSet<>();
				// podemos poner cualquiera, pero no se puede repetir en el
				// coleccion _set
				long idSong = 1L;
				songs.add(new Canciones(
						++idSong,
						"Call Me",
						"Blondie",
						"/android.resource://org.escoladeltreball/music/call_me.mp3",
						""));
				songs.add(new Canciones(
						++idSong,
						"The Look",
						"Roxete",
						"/android.resource://org.escoladeltreball/music/the_look.mp3",
						""));
				songs.add(new Canciones(
						++idSong,
						"Joyride",
						"Roxete",
						"/android.resource://org.escoladeltreball/music/joyride.mp3",
						""));

				songs.add(new Canciones(
						++idSong,
						"sultans of Swing",
						"Dire Straits",
						"/android.resource://org.escoladeltreball/music/sulansofswing_direstraits.mp3",
						""));

				favorite.setCanciones(songs);

				System.out.println("canciones to update............");
				for (Canciones canciones : songs) {
					System.out.println(canciones);

				}

				/**
				 * creamos el objeto FavoriteAndSongs, usando los objetos que
				 * hemos creado
				 */
				FavoriteAndSongs favAndSongs = new FavoriteAndSongs();
				favAndSongs.favorites = favorite;
				// Debemos especificarle el id del usuario
				favAndSongs.idUser = userId;
				favAndSongs.songs = new ArrayList<Canciones>(favorite
						.getCanciones());

				final String favoritesAndItsSongs = favAndSongs
						.serializeToJson();

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					paramsMap.put(HttpArgs.favoriteAndItsSongs,
							favoritesAndItsSongs);

					String responseString = UtilsHttp
							.performPostCall(
									TestConfig.SERVER_URL
											+ Config.HttpActions.UPDATE_FAVORITE_AND_ITS_SONGS,
									paramsMap);

					System.out
							.println("testupdateFavoritesAndItsSongs() response String from server : "
									+ responseString);

				} catch (Throwable e) {
					System.out.println("error" + e.toString());
					e.printStackTrace();
				} finally {
				}

			}
		}).start();
	}

	/**
	 * 
	 * @param responseString
	 * @param o
	 *            objeto del tipo a deserializar (en caso de exito)
	 * 
	 * @return un objeto del tipo de la clase pasada o StatusPackage si la
	 *         peticion al servidor no ha de devolver ningún objeto/colección en
	 *         particular; o null si no se puede parsear
	 * @throws Exception
	 */
	public static <T> Object parseResponseFromServer(String responseString,
			Object o) throws Exception {
		System.out.println("parseResponseFromServer()... ");
		if (responseString == null) {
			return null;
		}

		StatusPackage statusPackate = null;
		Gson gson = new GsonBuilder().create();
		try {
			statusPackate = gson.fromJson(responseString.toString(),
					new StatusPackage().getClass());

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (o != null) {
			if (statusPackate == null || statusPackate.getType() == null) {
				System.out.println("parsim custom class...");
				@SuppressWarnings("unchecked")
				T c = (T) gson.fromJson(responseString, o.getClass());
				return c;
			} else {
				return statusPackate;
			}
		}
		return null;
	}

	public static <T> T convertInstanceOfObject(Object o, Class<T> clazz) {
		try {
			return clazz.cast(o);
		} catch (ClassCastException e) {
			e.printStackTrace();
			return null;
		}
	}

}
