package org.escoladeltreball.model.json;
import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.model.persistence.Canciones;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;


public class ListSongs extends SerializableJson<ListSongs> implements
		Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	public List<Canciones> list;

	public ListSongs() {
	}

}