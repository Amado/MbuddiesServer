package org.escoladeltreball.model.json;

import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.model.persistence.Canciones;
import org.escoladeltreball.model.persistence.Favorites;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class FavoriteAndSongs extends SerializableJson<FavoriteAndSongs>
		implements Serializable {

	private static final long serialVersionUID = 1L;

	@Expose
	public Long idUser;
	@Expose
	public List<Canciones> songs;
	@Expose
	public Favorites favorites;

	public FavoriteAndSongs() {
	}

}