package org.escoladeltreball.model.json;

import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.model.persistence.Users;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

/**
 * clase que permite serializar un Conjunto de Usuarios
 */
public class ListUsers extends SerializableJson<ListUsers> implements
		Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	public List<Users> list;

	public ListUsers() {
	}

}
