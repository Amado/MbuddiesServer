package org.escoladeltreball.model.json;

import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class ListCountriesCities extends
		SerializableJson<ListCountriesCities> implements Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	public List<String> listCountries;
	@Expose
	public List<String> listCities;

	public ListCountriesCities() {
	}

}
