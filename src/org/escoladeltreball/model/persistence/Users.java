package org.escoladeltreball.model.persistence;


import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.escoladeltreball.support.SerializableJson;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "users", catalog = "music_friends_db", uniqueConstraints = { @UniqueConstraint(columnNames = "name") })
public class Users extends SerializableJson<Users> implements Serializable,
		Comparable<Users> {

	private static final long serialVersionUID = 1L;
	@Expose
	private Long id;
	@Expose
	private String name;
	private String password;
	@Expose
	private String city;
	@Expose
	private String country;
	@Expose
	private Boolean isConnected;
	@Expose
	private String personalDescription;
	@Expose
	private String musicPreferences;
	@Expose
	private byte[] photo;
	@Expose
	private String photoResource;
	@Expose
	private Integer idImg;
	@Expose
	private Canciones canciones;

	private Set<Favorites> favoriteses = new HashSet<Favorites>(0);

	private Set<FriendRequest> friendRequestsForIdUserSrc = new HashSet<FriendRequest>(
			0);
	private Set<FriendRequest> friendRequestsForIdUserTarget = new HashSet<FriendRequest>(
			0);

	public Users() {
	}

	public Users(Long id) {
		this.id = id;
	}


	private SortedSet<Users> owners = new TreeSet<Users>();

	private SortedSet<Users> friends = new TreeSet<Users>();

	@ManyToMany(mappedBy = "friends")
	@OrderBy("name ASC")
	public SortedSet<Users> getOwners() {
		return owners;
	}

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "friends_list", joinColumns = { @JoinColumn(name = "id_owner") }, inverseJoinColumns = { @JoinColumn(name = "id_friend") })
	@OrderBy("name ASC")
	public SortedSet<Users> getFriends() {
		return friends;
	}

	public void setOwners(SortedSet<Users> owners) {
		this.owners = owners;
	}

	public void setFriends(SortedSet<Users> friends) {
		this.friends = friends;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", name=" + name + ", password=" + password
				+ ", city=" + city + ", country=" + country + ", isConnected="
				+ isConnected + ", personalDescription=" + personalDescription
				+ ", musicPreferences=" + musicPreferences + ", photo="
				+ Arrays.toString(photo) + ", photoResource=" + photoResource
				+ ", idImg=" + idImg + "]";
	}

	public Users(String name, String password) {
		this.name = name;
		this.password = password;
	}

	public Users(String name) {
		this.name = name;
	}

	public Users(String name, String password, String city, String country,
			boolean isConnected) {
		this.name = name;
		this.password = password;
		this.city = city;
		this.country = country;
		this.isConnected = isConnected;
	}

	public Users(Canciones canciones, String name, String city, String country,
			Boolean isConnected, String personalDescription,
			String musicPreferences, byte[] photo, String password,
			String photoResource,
			Set<FriendRequest> friendRequestsForIdUserTarget,
			Set<Favorites> favoriteses,
			Set<FriendRequest> friendRequestsForIdUserSrc) {
		this.canciones = canciones;
		this.name = name;
		this.city = city;
		this.country = country;
		this.isConnected = isConnected;
		this.personalDescription = personalDescription;
		this.musicPreferences = musicPreferences;
		this.photo = photo;
		this.password = password;
		this.photoResource = photoResource;
		this.friendRequestsForIdUserTarget = friendRequestsForIdUserTarget;
		this.favoriteses = favoriteses;
		this.friendRequestsForIdUserSrc = friendRequestsForIdUserSrc;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
 
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Canciones.class)
	@JoinColumn(name = "id_current_play_song")
	public Canciones getCanciones() {
		return this.canciones;
	}

	public void setCanciones(Canciones canciones) {
		this.canciones = canciones;
	}

	@Column(name = "name", unique = true, nullable = false, length = 60)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "city", length = 50)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "country", length = 50)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "is_connected")
	public Boolean getIsConnected() {
		return this.isConnected;
	}

	public void setIsConnected(Boolean isConnected) {
		this.isConnected = isConnected;
	}

	@Column(name = "personal_description", length = 500)
	public String getPersonalDescription() {
		return this.personalDescription;
	}

	public void setPersonalDescription(String personalDescription) {
		this.personalDescription = personalDescription;
	}

	@Column(name = "music_preferences", length = 500)
	public String getMusicPreferences() {
		return this.musicPreferences;
	}

	public void setMusicPreferences(String musicPreferences) {
		this.musicPreferences = musicPreferences;
	}

	@Column(name = "photo")
	public byte[] getPhoto() {
		return this.photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	@Column(name = "password", nullable = false, length = 20)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "photo_resource", length = 65535)
	public String getPhotoResource() {
		return this.photoResource;
	}

	public void setPhotoResource(String photoResource) {
		this.photoResource = photoResource;
	}

	@Column(name = "id_img")
	public Integer getIdImg() {
		return idImg;
	}

	public void setIdImg(Integer idImg) {
		this.idImg = idImg;
	}

 
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usersByIdUserTarget")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	public Set<FriendRequest> getFriendRequestsForIdUserTarget() {
		return this.friendRequestsForIdUserTarget;
	}

	public void setFriendRequestsForIdUserTarget(
			Set<FriendRequest> friendRequestsForIdUserTarget) {
		this.friendRequestsForIdUserTarget = friendRequestsForIdUserTarget;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usersByIdUserSrc")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	public Set<FriendRequest> getFriendRequestsForIdUserSrc() {
		return this.friendRequestsForIdUserSrc;
	}

	public void setFriendRequestsForIdUserSrc(
			Set<FriendRequest> friendRequestsForIdUserSrc) {
		this.friendRequestsForIdUserSrc = friendRequestsForIdUserSrc;
	}

	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "users")
	public Set<Favorites> getFavoriteses() {
		return this.favoriteses;
	}

	public void setFavoriteses(Set<Favorites> favoriteses) {
		this.favoriteses = favoriteses;
	}

	@Override
	public int compareTo(Users o) {
		return this.name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
