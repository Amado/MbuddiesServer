package org.escoladeltreball.model.persistence;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "favorites", catalog = "music_friends_db")
public class Favorites implements java.io.Serializable, Comparable<Favorites> {

	private static final long serialVersionUID = 1L;
	@Expose
	private Long idList;
	@Expose
	private String nameOfList;
	@Expose
	private String identification;

	private Users users;

	private Set<Canciones> canciones;

	public Favorites() {
	}

	public Favorites(Long idList, String nameOfList, String identification,
			Users users, Set<Canciones> canciones) {
		super();
		this.idList = idList;
		this.nameOfList = nameOfList;
		this.identification = identification;
		this.users = users;
		this.canciones = canciones;
	}

	public Favorites(String identification, String nameOfList) {
		this.identification = identification;
		this.nameOfList = nameOfList;
	}

	public Favorites(Users users, String identification, String nameOfList) {
		this.users = users;
		this.identification = identification;
		this.nameOfList = nameOfList;
	}

	public Favorites(Users users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Favorites [idList=" + idList + ", users=" + users
				+ ", nameOfList=" + nameOfList + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_list", unique = true, nullable = false)
	public Long getIdList() {
		return this.idList;
	}

	/**
	 * elimina las canciones huerfanas al eliminar un "Favorito"
	 * 
	 * @return
	 */
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@OnDelete(action = OnDeleteAction.NO_ACTION)
	@JoinTable(name = "songs_and_list", catalog = "music_friends_db", joinColumns = { @JoinColumn(name = "id_list", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "id_song", nullable = false, updatable = false) })
	public Set<Canciones> getCanciones() {
		return canciones;
	}

	public void setCanciones(Set<Canciones> canciones) {
		this.canciones = canciones;
	}

	public void setIdList(Long idList) {
		this.idList = idList;
	}

	 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user", nullable = false)
	@Fetch(FetchMode.JOIN)
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name = "name_of_list", nullable = false, length = 25)
	public String getNameOfList() {
		return this.nameOfList;
	}

	public void setNameOfList(String nameOfList) {
		this.nameOfList = nameOfList;
	}

	@Column(name = "identification", nullable = false, length = 50, unique = true)
	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	@Override
	public int compareTo(Favorites o) {
		return (this.nameOfList).toLowerCase().compareTo(
				(o.nameOfList).toLowerCase());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idList == null) ? 0 : idList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favorites other = (Favorites) obj;
		if (idList == null) {
			if (other.idList != null)
				return false;
		} else if (!idList.equals(other.idList))
			return false;
		return true;
	}

}
