package org.escoladeltreball.model.persistence;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.escoladeltreball.support.SerializableJson;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "canciones", catalog = "music_friends_db")
public class Canciones extends SerializableJson<Users> implements
		java.io.Serializable, Comparable<Canciones> {

	private static final long serialVersionUID = 1L;
	@Expose
	private Long idSong;
	@Expose
	private String songName;
	@Expose
	private String songAuthor;
	@Expose
	private String srcPath;
	@Expose
	private Integer idSrc;
	@Expose
	private byte[] photo;
	@Expose
	private String photoEncoded;

	private Set<Users> userses = new HashSet<Users>(0);

	private Set<Favorites> favorites;

	public Canciones() {
	}

	public Canciones(Long idSong, String songName, String songAuthor,
			String srcPath, String photoEncoded) {
		this.idSong = idSong;
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
		this.photoEncoded = photoEncoded;
	}

	public Canciones(String songName, String songAuthor, String srcPath,
			String photoEncoded) {
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
		this.photoEncoded = photoEncoded;
	}

	public Canciones(String songName, String songAuthor, String srcPath,
			byte[] photo) {
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
		this.photo = photo;
	}

	public Canciones(String songName) {
		this.songName = songName;
	}

	public Canciones(String songName, String songAuthor, String srcPath,
			Set<Users> userses) {
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
		this.userses = userses;
	}
 

	@Override
	public String toString() {
		return "Canciones [idSong=" + idSong + ", songName=" + songName
				+ ", songAuthor=" + songAuthor + ", srcPath=" + srcPath
				+ ", idSrc=" + idSrc + ", photo=" + Arrays.toString(photo)
				+ ", photoEncoded=" + photoEncoded + "]";
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_song", unique = true, nullable = false)
	public Long getIdSong() {
		return this.idSong;
	}

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "canciones")
	@Fetch(FetchMode.JOIN)
	public Set<Favorites> getFavorites() {
		return this.favorites;
	}

	public void setFavorites(Set<Favorites> favorites) {
		this.favorites = favorites;
	}

	public void setIdSong(Long idSong) {
		this.idSong = idSong;
	}

	@Column(name = "song_name", nullable = false, length = 256)
	public String getSongName() {
		return this.songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	@Column(name = "song_author", length = 50)
	public String getSongAuthor() {
		return this.songAuthor;
	}

	public void setSongAuthor(String songAuthor) {
		this.songAuthor = songAuthor;
	}

	@Column(name = "src_path", length = 256)
	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	@Column(name = "id_src")
	public Integer getIdSrc() {
		return idSrc;
	}

	public void setIdSrc(Integer idSrc) {
		this.idSrc = idSrc;
	}

	@Column(name = "photo")
	public byte[] getPhoto() {
		return this.photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	@Column(name = "photo_encoded")
	public String getPhotoEncoded() {
		return photoEncoded;
	}

	public void setPhotoEncoded(String photoEncoded) {
		this.photoEncoded = photoEncoded;
	}

	/**
	 * Elimina las canciones huerfanas al eliminar un "Favorito"
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "canciones")
	public Set<Users> getUserses() {
		return this.userses;
	}

	public void setUserses(Set<Users> userses) {
		this.userses = userses;
	}

	@Override
	public int compareTo(Canciones o) {
		return (this.songName + this.songAuthor).toLowerCase().compareTo(
				(o.songName + o.songAuthor).toLowerCase());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSong == null) ? 0 : idSong.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Canciones other = (Canciones) obj;
		if (idSong == null) {
			if (other.idSong != null)
				return false;
		} else if (!idSong.equals(other.idSong))
			return false;
		return true;
	}

}
