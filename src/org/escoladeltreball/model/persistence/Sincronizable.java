package org.escoladeltreball.model.persistence;

import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

/**
 * solo es necesario tener un seguimiento de las entidades no sincronizadas en
 * el cliente.
 *
 */
public interface Sincronizable {
	@Transient
	public static final String SINC = "SINC";
	@Transient
	public static final String DESYNC = "DESYNC";

	@Expose
	public String sincronizado = DESYNC;

}
