package org.escoladeltreball.model.persistence;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "friend_request", catalog = "music_friends_db")
public class FriendRequest extends SerializableJson<FriendRequest> implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	private Integer id;
	@Expose
	private Users usersByIdUserTarget;
	@Expose
	private Users usersByIdUserSrc;
	@Expose
	private String status;

	public FriendRequest() {
	}

	public FriendRequest(Users usersByIdUserTarget, Users usersByIdUserSrc,
			String status) {
		this.usersByIdUserTarget = usersByIdUserTarget;
		this.usersByIdUserSrc = usersByIdUserSrc;
		this.status = status;
	}

	@Override
	public String toString() {
		return "FriendRequest [id=" + id + ", usersByIdUserTarget="
				+ usersByIdUserTarget + ", usersByIdUserSrc="
				+ usersByIdUserSrc + ", status=" + status + "]";
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_user_target", nullable = false)
	public Users getUsersByIdUserTarget() {
		return this.usersByIdUserTarget;
	}

	public void setUsersByIdUserTarget(Users usersByIdUserTarget) {
		this.usersByIdUserTarget = usersByIdUserTarget;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_user_src", nullable = false)
	public Users getUsersByIdUserSrc() {
		return this.usersByIdUserSrc;
	}

	public void setUsersByIdUserSrc(Users usersByIdUserSrc) {
		this.usersByIdUserSrc = usersByIdUserSrc;
	}

	@Column(name = "status", nullable = false, length = 1)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((usersByIdUserSrc == null) ? 0 : usersByIdUserSrc.hashCode());
		result = prime
				* result
				+ ((usersByIdUserTarget == null) ? 0 : usersByIdUserTarget
						.hashCode());
		return result;
	}

	/**
	 *  
	 *  
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendRequest other = (FriendRequest) obj;
		if (usersByIdUserSrc == null) {
			if (other.usersByIdUserSrc != null) {
				return false;
			}
		} else if (!usersByIdUserSrc.equals(other.usersByIdUserSrc)) {
			return false;
		}
		if (usersByIdUserTarget == null) {
			if (other.usersByIdUserTarget != null) {
				return false;
			}
		} else if (!usersByIdUserTarget.equals(other.usersByIdUserTarget)) {
			return false;
		}
		if (usersByIdUserSrc == null) {
			if (other.usersByIdUserTarget != null) {
				return false;
			}
		} else if (!usersByIdUserTarget.equals(other.usersByIdUserSrc)) {
			return false;
		}
		if (usersByIdUserTarget == null) {
			if (other.usersByIdUserSrc != null) {
				return false;
			}
		} else if (!usersByIdUserTarget.equals(other.usersByIdUserSrc)) {
			return false;
		}
		return true;
	}

}
