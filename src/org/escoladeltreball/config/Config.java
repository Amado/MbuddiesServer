package org.escoladeltreball.config;

/**
 * 
 * clase de configuracion del servidor
 *
 */
public interface Config {

	/**
	 * JSON objects attributes
	 */
	public static final String JSON_OB_TYPE_ERROR = "ERROR";
	public static final String JSON_OB_TYPE_SUCCESS = "SUCCESS";

	public static final String TAG_INFO = "INFO", TAG_ERROR = "ERROR";
	public static final String CHARACTER_ENCODING = "UTF-8";

	public static final String femeninoStr = "f";
	public static final String masculinoStr = "m";
	public static final boolean femenino = true;
	public static final boolean masculino = false;

	/*
	 * <SHARED VARIABLES BEETWEN SERVER AND CLIENT COMMUNICATION>
	 */
	/**
	 * variables con los parametros que se pueden parsar al servidor al realizar
	 * una peticion
	 */
	public static interface HttpArgs {

		public static final String VALUE_ALL = "all";
		public static final String VALUE_TRUE = "TRUE";
		public static final String VALUE_FALSE = "FALSE";
		/*
		 * Users attribures
		 */
		// public static final String latitude = "latitude";
		// public static final String longitude = "longitude";
		public static final String idUser = "idUser";
		public static final String username = "username";
		// public static final String mail = "mail";
		public static final String password = "password";
		public static final String city = "city";
		public static final String country = "country";
		public static final String personalDescription = "personalDescription";
		public static final String musicPreferences = "musicPreferences";
		public static final String photo = "photo";
		public static final String idPhoto = "idPhoto";
		//
		public static final String withSongObject = "withSongObject";
		// public static final String photoResource = "photoResource";

		/*
		 * favorites to identify a song y server db: the identify field instead
		 * of idFavorite
		 */
		// public static final String idFavorites = "idFavorites";
		public static final String favoritesName = "favoritesName";
		public static final String favoritesNewName = "favoritesNewName";
		public static final String favoritesIdentification = "favoritesIdentification";
		public static final String favoriteAndItsSongs = "favoriteAndItsSongs";
		public static final String favoritesList = "favoritesList";

		/*
		 * songs attributes to identify a song y server db: is necessary the
		 * userId and the songSrcPath
		 */
		// public static final String idSong = "idSong";
		public static final String songName = "songName";
		public static final String songAuthor = "songAuthor";
		public static final String songSrcPath = "songSrcPath";
		public static final String songPhotoEncoded = "songPhotoEncoded";
		/*
		 * users_friends
		 */
		public static final String idUserFriend = "idUserFriend";
		public static final String idUserOwner = "idUserOwner";
	}

	/**
	 * variables que establecen las peticiones que el servidor puede responder
	 */
	public static interface HttpActions {
		/*
		 * Users
		 */
		public static final String LOGIN = "login";
		public static final String SIGNUP = "signUp";
		public static final String UPDATE_USER_PROFILE = "updateProfile";
		public static final String REMOVE_USER = "removeUser";
		//
		public static final String FIND_ALL_USERS = "findAllUsers";
		public static final String FIND_USER = "findUser";
		public static final String FIND_USERS_BY_COUNTRY_AND_CITY = "findUsersByCountryAndCity";
		public static final String UPDATE_CURRENT_PLAY_SONG = "findCurrentPlaySong";
		public static final String FIND_DISTINCT_COUNTRIES_AND_CITIES = "findDistinctCountriesAndCities";
		public static final String FIND_DISTINCT_CITIES_FROM_COUNTRY = "findDistinctCitiesFromCountry";
		public static final String FIND_FRIENDS_FROM_USER = "findFriendsFromUsers";
		//
		public static final String TEST = "test"; // to debug
		public static final String CREATE_TEST_DATA = "createTestData";

		/*
		 * Friends (idenficiation of users by name instead of their id)
		 */
		public static final String SEND_FRIEND_REQUEST = "sendFriendRequest";
		public static final String DISMIS_FRIEND_REQUEST = "dismisFriendRequest";
		public static final String RESPONSE_FRIEND_REQUEST = "responseFriendRequest";
		//
		public static final String FIND_FRIENDS_REQUEST = "findFriendsRequest";
		public static final String REMOVE_FRIEND = "removeFriend";
		public static final String ADD_FRIEND = "addFriend";

		/*
		 * Songs / Favorites
		 */
		public static final String UPDATE_SONG = "updateSong";
		public static final String ADD_SONG_TO_FAVORITE = "addSongToFavorite";
		public static final String REMOVE_SONG_FROM_FAVORITE = "removeSongFromFavorite";
		// ADD_OR_UPDATE_FAVORITE -> (only updates its name)
		public static final String ADD_OR_UPDATE_FAVORITE = "addUpdateFavorite";
		public static final String UPDATE_FAVORITE_AND_ITS_SONGS = "updateFavoriteAndItsSongs";
		public static final String COMMIT_FAVORITES = "commitFavorites";
		// REMOVE_FAVORITE -> (only removes the orphans songs)
		public static final String REMOVE_FAVORITE = "removeFavorite";

		// FIND_SONGS -> params: [ user | favorite ]
		public static final String FIND_SONGS = "findSongs";
		public static final String FIND_FAVORITES_FROM_USER = "findFavoritesFromUser";

	}
	/*
	 * </SHARED VARIABLES BEETWEN SERVER AND CLIENT COMMUNICATION>
	 */

}