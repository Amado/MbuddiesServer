package org.escoladeltreball.manager;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.escoladeltreball.config.Config;
import org.escoladeltreball.dao.FavoritesDAO;
import org.escoladeltreball.dao.FriendRequestDAO;
import org.escoladeltreball.dao.MockDAO;
import org.escoladeltreball.dao.SongsDAO;
import org.escoladeltreball.dao.UsersDAO;
import org.escoladeltreball.model.json.FavoriteAndSongs;
import org.escoladeltreball.model.json.ListCountriesCities;
import org.escoladeltreball.model.json.ListFavorites;
import org.escoladeltreball.model.json.ListFriendRequest;
import org.escoladeltreball.model.json.ListFriendRequest.RequestModel;
import org.escoladeltreball.model.json.ListSongs;
import org.escoladeltreball.model.json.ListUsers;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.persistence.Canciones;
import org.escoladeltreball.model.persistence.Favorites;
import org.escoladeltreball.model.persistence.FriendRequest;
import org.escoladeltreball.model.persistence.Users;
import org.escoladeltreball.servlet.SocketServlet;
import org.escoladeltreball.support.ImageConverter;
import org.hibernate.CacheMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.metamodel.ValidationException;

public class Manager implements Config {
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
	private static Manager instance;

	private Manager() {

	}

	public static Manager getIntance() {
		if (instance == null) {
			instance = new Manager();
		}
		return instance;
	}

	/**
	 * 
	 * obs: un usuario puede logearse ingresando el username comprueba si existe
	 * modifica el atributo isConnected del usuario y actualiza sus coordenadas
	 * 
	 * params: password, username, city, country
	 * 
	 * @return el objecto user o null si el login es incorrecto
	 */
	public Users processLogin(HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("processLogin begin...");
		final UsersDAO usersDAO = new UsersDAO();

		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong username/mail or password")
				.serializeToJson();

		// cogemos los parametros de la peticion
		Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();

			String username = request.getParameter(HttpArgs.username);
			String password = request.getParameter(HttpArgs.password);
			String city = request.getParameter(HttpArgs.city);
			String country = request.getParameter(HttpArgs.country);

			logger.info(String.format(
					"username: %s, password: %s, city %s, country %s",
					username, password, city, country));

			// caso de error: pass invalida
			if (password == null || password.isEmpty()) {
				throw new ValidationException("invalid password");
			}

			usersDAO.setSession(usersDAO.getDAOManager());

			// logeamos en con el nombre
			// valido
			if (username != null && username.isEmpty() == false) {
				user = usersDAO
						.getUserByUsernameAndPassword(username, password);
			}

			// caso de error: usuario no encontrado
			if (user == null) {
				throw new ValidationException("User: " + username
						+ " not found ");
			}

			// caso de exito:
			// actualizamos la ciudad y el pais solo si no es null o no no
			// contenga ningun valor
			user.setIsConnected(true);
			user.setCanciones(null);

			if (city != null && city.isEmpty() == false) {
				user.setCity(city);
			}

			if (country != null && country.isEmpty() == false) {
				user.setCountry(country);
			}

			// actualizamos el usuario en la base de datos
			logger.info("update user...");
			usersDAO.update(user);

			// en caso de exito devolvemos los datos del usuario
			String userJson = user.serializeToJson();
			out.print(userJson);
			// response.setStatus(HttpServletResponse.SC_OK);
			logger.info("processLogin() -OK");
			logger.info("response =  " + userJson);
		} catch (ValidationException e) {
			logger.info("processLogin() -ERROR -> ValidationException"
					+ e.getMessage());
			String validationError = new StatusPackage(
					StatusPackage.STATUS_ERROR, e.getMessage())
					.serializeToJson();
			out.print(validationError);
			logger.info("processLogin() response =  " + validationError);
		} catch (IOException e) {
			logger.info("processLogin() -ERROR -> IOException" + e.getMessage());
			e.printStackTrace();
			// out.print(errorJsonObj);
			logger.info("processLogin() response =  " + errorJsonObj);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("processLogin() -ERROR -> Exception");
			logger.info("processLogin() response =  " + errorJsonObj);
			out.print(errorJsonObj);
			// response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
		return user;

	}

	public Users processSignUp(HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("processSignUp begin...");
		// cogemos los parametros de la peticion
		final UsersDAO usersDAO = new UsersDAO();
		Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			String username = request.getParameter(HttpArgs.username);
			String password = request.getParameter(HttpArgs.password);
			String city = request.getParameter(HttpArgs.city);
			String country = request.getParameter(HttpArgs.country);

			usersDAO.setSession(usersDAO.getDAOManager());
			user = new Users(username, password, city, country, true);

			usersDAO.save(user);

			// en caso de exito, notificamos al cliente
			logger.info("procesSignUp() created user " + user);
			String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS, null).serializeToJson();

			// en caso de exito devolvemos los datos del usuario
			String userJson = user.serializeToJson();
			out.print(userJson);
			// out.print(successJson);
		} catch (ConstraintViolationException e) {
			logger.info("processLogin() -ERROR -> ConstraintViolationException");
			e.printStackTrace();
			String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
					"Error, there is already an user with the same username")
					.serializeToJson();
			out.print(errorJsonObj);
		} catch (Exception e) {
			logger.info("processLogin() -ERROR -> Exception");
			e.printStackTrace();
			String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
					"Error performing Sign Up").serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}
		return user;
	}

	/**
	 * comprueba si existe modifica el atributo isConnected del usuario y
	 * actualiza sus coordenadas
	 * 
	 * @return null si la consulta es erronea
	 */
	@SuppressWarnings("unused")
	public List<Users> processFindAllUsers(HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("processFindAllUsers begin...");
		final UsersDAO usersDAO = new UsersDAO();
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Error al consultar los usuarios")
				.serializeToJson();

		ListUsers listUsers = new ListUsers();
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}

			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_
			usersDAO.setSession(usersDAO.getDAOManager());
			listUsers.list = usersDAO.findAll();

			// ---- mostramos si estan conectados al servidor ------------
			if (listUsers.list != null) {
				for (Users userN : listUsers.list) {
					final boolean isConnected = SocketServlet
							.isConnectedUserId(String.valueOf(userN.getId()));
					userN.setIsConnected(isConnected);
				}

			}

			// caso de error
			if (listUsers == null) {
				out.print(errorJsonObj);
			}

			// convertimos la lista de usuarios en un objeto Json
			String listUsersJson = listUsers.serializeToJson();

			// devolvemos el resultado
			out.print(listUsersJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			logger.info("processLogin() -> Exception");
			e.printStackTrace();
			out.print(errorJsonObj);
			// response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession(); // _cerramos la sesion_
		}
		return listUsers.list;
	}

	public void processUpdateUserProfile(HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("processUpdateUserProfile begin...");
		final UsersDAO usersDAO = new UsersDAO();

		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// cogemos los parametros de la peticion
		// Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			String id = request.getParameter(HttpArgs.idUser);
			String username = request.getParameter(HttpArgs.username);
			String password = request.getParameter(HttpArgs.password);
			String city = request.getParameter(HttpArgs.city);
			String country = request.getParameter(HttpArgs.country);
			String personalDescription = request
					.getParameter(HttpArgs.personalDescription);
			String musicPreferences = request
					.getParameter(HttpArgs.musicPreferences);
			String photo = request.getParameter(HttpArgs.photo);
			// String photoResource =
			// request.getParameter(HttpArgs.photoResource);
			Integer idPhoto = Integer.parseInt(request
					.getParameter(HttpArgs.idPhoto));

			// recuperamos el usuario de la base de datos y lo actualizamos:
			usersDAO.setSession(usersDAO.getDAOManager());
			List<Users> usersFound = usersDAO.getUsersById(Long.parseLong(id));
			// usersDAO.closeSession();

			// caso de error:
			if (usersFound == null || usersFound.isEmpty()) {
				out.print(errorJsonObj);
				return;
			}

			Users user = usersFound.get(0);

			// actualizamos los atributos del usuario
			user.setName(username);
			user.setCity(city);
			user.setCountry(country);
			user.setPassword(password);
			user.setPersonalDescription(personalDescription);
			user.setMusicPreferences(musicPreferences);
			// decodificamos la foto que nos llega como un string
			byte[] decodedPhoto = ImageConverter.decodeImage(photo);
			user.setPhoto(decodedPhoto);
			// user.setPhotoResource(photoResource);
			user.setIdImg(idPhoto);

			logger.info("user to persist  " + user);
			//
			// actualizamos el usuario en la base de datos
			logger.info("update user...");
			usersDAO.setSession(usersDAO.getDAOManager());
			usersDAO.update(user);

			// logging
			String userJson = user.serializeToJson();
			out.print(userJson);
			logger.info("user updated: \n" + userJson);
			logger.info("processUpdateUserProfile() -OK");

		} catch (ConstraintViolationException e) {
			logger.info("processLogin() -ERROR -> ConstraintViolationException");
			e.printStackTrace();
			String errorJsonObjTmp = new StatusPackage(
					StatusPackage.STATUS_ERROR,
					"Error performing Sign Up - there are already an user with the same username")
					.serializeToJson();
			out.print(errorJsonObjTmp);
		} catch (Exception e) {
			logger.info("processUpdateUserProfile() -> Exception");
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
	}

	public void processRemoveUser(HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("processRemoveUser begin...");
		// cogemos los parametros de la peticion
		final UsersDAO usersDAO = new UsersDAO();
		FavoritesDAO favoritesDAO = new FavoritesDAO();
		final SongsDAO songsDAO = new SongsDAO();
		FriendRequestDAO friendRequestDAO = new FriendRequestDAO();
		Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			String username = request.getParameter(HttpArgs.username);

			usersDAO.setSession(usersDAO.getDAOManager());

			try {
				user = usersDAO.getUsersByName(username).get(0);

			} catch (Exception e) {
				final String errorJsonObj = new StatusPackage(
						StatusPackage.STATUS_ERROR, "Error, the user "
								+ username + " doesn't exits")
						.serializeToJson();
				out.print(errorJsonObj);
				return;
			}

			songsDAO.setSession(usersDAO.getSession());
			favoritesDAO.setSession(usersDAO.getSession());
			friendRequestDAO.setSession(usersDAO.getSession());

			/**
			 * not neccesary since favorites has CascadeType.DELETE_ORPHAN
			 */
			// Hibernate.initialize(user.getFavoriteses());
			// if (user.getFavoriteses() != null) {
			// for (Favorites favoriteN : user.getFavoriteses()) {
			// Hibernate.initialize(favoriteN.getCanciones());
			// }
			// }
			// logger.info("canciones favSongsFromDb:");
			// for (Canciones canciones : favSongsFromDb) {
			// logger.info(" " + canciones);
			// }

			// eliminamos las canciones del usuario
			List<Canciones> songsFromUser = songsDAO.getSongsByUser(user
					.getId());

			if (songsFromUser != null && songsFromUser.isEmpty() == false) {
				for (Canciones songFDBN : songsFromUser) {

					try {
						songsDAO.getSession().delete(songFDBN);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			/*
			 * 
			 * to be able to delete the user without broke the constrains with
			 * favorites we msut to remove their favorites since setting
			 * user.favorites (cascade All) embraces others users that will be
			 * deleted also.
			 */
			logger.info("***************removing favorites from user ****************");
			List<Favorites> favorites = favoritesDAO.getFavoritesByIdUser(user
					.getId());

			if (favorites != null) {
				for (Favorites favoritesN : favorites) {
					// logger.info("removing favorite... **identification: "
					// + favoritesN.getIdentification() + " **");
					favoritesDAO.getSession().delete(favoritesN);
				}
			}

			// List<Favorites> favorites =
			List<FriendRequest> friendRequests = friendRequestDAO
					.getRequestByUserTargetOrSource(user.getId(), user.getId());

			logger.info("*************** Removing friendRequests from user or to .... ***************");
			if (friendRequests != null) {
				for (FriendRequest fRequetN : friendRequests) {
					logger.info("removing friendRequest n: src: "
							+ fRequetN.getUsersByIdUserSrc().getId() + " trg: "
							+ fRequetN.getUsersByIdUserTarget().getId());
					friendRequestDAO.getSession().delete(fRequetN);
				}
			} else {
				logger.info("there isn't any friendRequest from or to the user ....");
			}
			logger.info("setting to null favoritess, canciones, setFriendRequestsForIdUserSrc and setFriendRequestsForIdUserTarget  from user");
			user.setFavoriteses(null);
			user.setCanciones(null);
			user.setFriendRequestsForIdUserSrc(null);
			user.setFriendRequestsForIdUserTarget(null);
			logger.info("deleting user");
			// usersDAO.getSession().delete(user);

			usersDAO.delete(user);

			logger.info("flushing the session");
			favoritesDAO.getSession().flush();

			//
			// usersDAO.delete(user);

			// en caso de exito, notificamos al cliente
			logger.info("processRemoveUser() created user " + user);
			String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS, "the user " + username
							+ " was removed successful").serializeToJson();
			out.print(successJson);
		} catch (ConstraintViolationException e) {
			logger.info("processLogin() -ERROR -> ConstraintViolationException");
			e.printStackTrace();
			String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
					"Error performing processRemoveUser - ConstraintViolationException")
					.serializeToJson();
			out.print(errorJsonObj);
		} catch (Exception e) {
			logger.info("processRemoveUser() -ERROR -> Exception");
			e.printStackTrace();
			String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
					"Error performing processRemoveUser").serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
	}

	/**
	 * para identificar una cancion del cliente en la base de datos del servidor
	 * no podemos usar su id; el idUsuario y el nombre de la lista por lo que no
	 * se permiten canciones con mismo nombre y autor en una misma lista, ni dos
	 * listas con el mismo nombre.
	 * 
	 * @param request
	 * @param response
	 */
	public void processUpdateCurrentPlaySong(HttpServletRequest request,
			HttpServletResponse response) {

		final SongsDAO songsDAO = new SongsDAO();
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processUpdateCurrentPlaySong begin...");

		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// cogemos los parametros de la peticion
		// Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			Long idUser = Long.parseLong(request.getParameter(HttpArgs.idUser));
			String songSrcPath = request.getParameter(HttpArgs.songSrcPath);
			String songName = request.getParameter(HttpArgs.songName);
			String songAuthor = request.getParameter(HttpArgs.songAuthor);

			logger.info("searching... user and song");

			// recuperamos el usuario de la base de datos y lo actualizamos:
			songsDAO.setSession(songsDAO.getDAOManager());
			usersDAO.setSession(songsDAO.getSession());

			Users user = usersDAO.getUsersById(idUser).get(0);

			Canciones currentPlaySong = null;
			if (songName != null) {

				List<Canciones> songsFound = songsDAO
						.getSongBySrcPath(songSrcPath);

				// si no se encuentra la cancion la creamos
				if (songsFound == null || songsFound.isEmpty()) {
					// final String errorNotSongFound = new StatusPackage(
					// StatusPackage.STATUS_ERROR,
					// "Wrong request, not song Found").serializeToJson();
					// out.print(errorNotSongFound);
					// public Canciones(String songName, String songAuthor,
					// Set<Users> userses)
					Set<Users> tmpUsers = new HashSet<Users>();
					tmpUsers.add(user);

					/**
					 * String songName, String songAuthor, String srcPath,
					 * Set<Users> userses
					 */

					currentPlaySong = new Canciones(songName, songAuthor,
							songSrcPath, tmpUsers);
					songsDAO.save(currentPlaySong);

				} else {
					for (Canciones canciones : songsFound) {
						// if(canciones.get){
						//
						// }
					}

					currentPlaySong = songsFound.get(0);

					if (songsFound.size() > 1) {
						logger.warning("processUpdateCurrentPlaySong... "
								+ "WARNING!! there are one or more songs with de same songSrcPath belonging to the same user!!!: srcpath = '"
								+ songSrcPath + "' idUser = " + idUser);
					}
				}
			}

			user.setCanciones(currentPlaySong);
			usersDAO.update(user);

			// logging
			String songJson = currentPlaySong.serializeToJson();
			out.print(songJson);
			logger.info("processUpdateSong() -OK");

		} catch (ConstraintViolationException e) {
			logger.info("processLogin() -ERROR -> ConstraintViolationException");
			e.printStackTrace();
			String errorJsonObjTmp = new StatusPackage(
					StatusPackage.STATUS_ERROR,
					"Error performing Sign Up - the name or the author of the songs are invalid")
					.serializeToJson();
			out.print(errorJsonObjTmp);
		} catch (Exception e) {
			logger.info("processUpdateCurrentPlaySong() -> Exception");
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			songsDAO.closeSession();
		}
	}

	/**
	 * busca un usuario en la base de datos por id o por name, en funcion de los
	 * parametros pasados
	 */
	public Users processFindUser(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processFindUser begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		Users user = new Users();
		PrintWriter out = null;
		List<Users> usersFound = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}

			Long id = null;
			String name = null;

			boolean withSongObject = withSongObject = HttpArgs.VALUE_TRUE
					.equals(request.getParameter(HttpArgs.withSongObject));
			try {
				// cogemos los parametros de la peticion
				id = Long.valueOf(request.getParameter(HttpArgs.idUser));
				//

			} catch (Exception e) {

				e.printStackTrace();
				name = request.getParameter(HttpArgs.username);
				if (name == null) {
					// caso de Error: los parametros pasados no son validos
					out.print(errorJsonObj);
					return null;
				}
			}

			usersDAO.setSession(usersDAO.getDAOManager());
			if (id != null) {
				// creamos un objeto de la clase ListUsers a partir del
				// resultado de
				// la consulta para serializaro
				// _open the session before_
				usersFound = usersDAO.getUsersById(id);

			} else {
				usersFound = usersDAO.getUsersByName(name);
			}

			// caso de error:
			if (usersFound == null || usersFound.isEmpty()) {
				out.print(errorJsonObj);
				logger.severe("no user found !");
				return null;
			}

			// comprobamos si hay mas de un usuario con el mismo username
			if (usersFound.size() > 1) {
				logger.warning("processFindUser... "
						+ "WARINIG!! there are one or more users with de same id!!: "
						+ id);
			}

			// devolvemos el usuario con el solo con el nombre de la cancion
			// para no enviar tambien la imagen de la cancion cuando la peticion
			// no indique lo contrario
			// con el parametro withSongObject
			user = usersFound.get(0);

			try {
				if (withSongObject == false) {
					Canciones tmpCanciones = new Canciones();
					tmpCanciones.setSongName(user.getCanciones().getSongName());
					user.setCanciones(tmpCanciones);
				}

			} catch (Exception e) {
				// si no tiene una cancion escuchando no hacemos nada
				// do nothing
			}

			// serializamos el objeto usuario en formato Json
			String userJson = user.serializeToJson();

			// devolvemos el resultado
			out.print(userJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			try {
				Session session = usersDAO.getSession();
				if (session != null && session.isOpen()) {
					// session.flush();
					session.close();

				}
			} catch (SessionException e) {
				e.printStackTrace();
			}
			// usersDAO.closeSession(); // _cerramos la sesion_
		}
		return user;

	}

	public void processFriendsFromUser(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processFriendsFromUser begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		Users user = new Users();
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			Long id = null;
			try {
				// cogemos los parametros de la peticion
				id = Long.valueOf(request.getParameter(HttpArgs.idUser));

			} catch (Exception e) {
				out.print(errorJsonObj);
				e.printStackTrace();
				return;
			}

			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_
			ListUsers listUsers = new ListUsers();
			usersDAO.setSession(usersDAO.getDAOManager());
			// usersDAO.getSession().setCacheMode(CacheMode.IGNORE);

			listUsers.list = usersDAO.getUsersByFriendId(id);

			if (listUsers.list != null) {
				for (Users userN : listUsers.list) {
					final boolean isConnected = SocketServlet
							.isConnectedUserId(String.valueOf(userN.getId()));
					userN.setIsConnected(isConnected);
				}

			}

			// caso de error
			if (listUsers == null) {
				out.print(errorJsonObj);
			}

			// convertimos la lista de usuarios en un objeto Json
			String listUsersJson = listUsers.serializeToJson();

			// devolvemos el resultado
			out.print(listUsersJson);
			response.setStatus(HttpServletResponse.SC_OK);

		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession(); // _cerramos la sesion_
		}

	}

	public void processFindUserByCountryAndCity(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processFindUserByCityAndCountry begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			String country = request.getParameter(HttpArgs.country);
			String city = request.getParameter(HttpArgs.city);

			// caso de error:
			if (city == null && country == null) {
				out.print(errorJsonObj);
				return;
			}

			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_
			usersDAO.setSession(usersDAO.getDAOManager());
			ListUsers listUsers = new ListUsers();
			if (country.equals(HttpArgs.VALUE_ALL)) {
				logger.info("*****search all Countries*****");
				country = "%";
			}
			if (city.equals(HttpArgs.VALUE_ALL)) {
				logger.info("*****search all Cities*****");
				city = "%";
			}

			listUsers.list = usersDAO.getUsersByCountryAndCity(country, city);

			// serializamos el objeto usuario en formato Json
			String listUsersJson = listUsers.serializeToJson();

			// devolvemos el resultado
			out.print(listUsersJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession(); // _cerramos la sesion_
		}
	}

	/**
	 * devuelve un objeto que contine 2 conjuntos con todas las ciudades y
	 * paises
	 */
	public void processFindDistinctCountriesAndCities(
			HttpServletRequest request, HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("getDistinctCountriesAndCities begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}
			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_
			usersDAO.setSession(usersDAO.getDAOManager()); // abrimos la sesion
			ListCountriesCities listCountriesCities = new ListCountriesCities();
			listCountriesCities.listCities = usersDAO.getDistinctCities();
			listCountriesCities.listCountries = usersDAO.getDistinctCountries();
			// serializamos el objeto usuario en formato Json
			String listCitiesAndCountries = listCountriesCities
					.serializeToJson();

			logger.info("returning the distinct cities and countries form DB");
			logger.info("listCitiesAndCountries " + listCitiesAndCountries);

			// devolvemos el resultado
			out.print(listCitiesAndCountries);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession(); // _cerramos la sesion_
		}
	}

	/**
	 * devuelve todas las ciudades de un pais concreto
	 * 
	 */
	public void procesFindDistinctCitiesFromCountry(HttpServletRequest request,
			HttpServletResponse response) {

		final UsersDAO usersDAO = new UsersDAO();
		logger.info("procesFindDistinctCitiesFromCountry begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}
			final String country = request.getParameter(HttpArgs.country);
			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_
			usersDAO.setSession(usersDAO.getDAOManager()); // abrimos la sesion
			ListCountriesCities listCountriesCities = new ListCountriesCities();

			if (country == "" || country.equals(Config.HttpArgs.VALUE_ALL)) {
				listCountriesCities.listCities = usersDAO.getDistinctCities();
			} else {
				listCountriesCities.listCities = usersDAO
						.getDistinctCitiesFromCountries(country);

			}
			// serializamos el objeto usuario en formato Json
			String listCitiesAndCountries = listCountriesCities
					.serializeToJson();

			logger.info("returning the distinct cities from country form DB");
			logger.info("listCitiesAndCountries " + listCitiesAndCountries);

			// devolvemos el resultado
			out.print(listCitiesAndCountries);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession(); // _cerramos la sesion_
		}
	}

	/**
	 * devuelve todas las peticiones de amistad para un usuario concreto
	 * 
	 * @param request
	 * @param response
	 */
	public void processFindFriendRequest(HttpServletRequest request,
			HttpServletResponse response) {

		final FriendRequestDAO fRequestDAO = new FriendRequestDAO();
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processFindFriendRequest begin...");
		String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
				"Wrong request").serializeToJson();

		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}
			Session session = usersDAO.getDAOManager();
			session.setCacheMode(CacheMode.IGNORE);
			fRequestDAO.setSession(session);
			usersDAO.setSession(session);
			// usersDAO.getSession().setCacheMode(CacheMode.IGNORE);
			Long userId = Long.valueOf(request.getParameter(HttpArgs.idUser));

			logger.info("Searching...  friendsRequest to user whith id: "
					+ userId + " on database");

			Users myUser = usersDAO.getUsersById(userId).get(0);

			List<FriendRequest> tmpFriendReq = new ArrayList<FriendRequest>();
			tmpFriendReq = fRequestDAO.getRequestByUserTarget(myUser.getId(),
					RequestModel.REQUEST);

			// buscamos las peticiones de amistad que se encuentren en la base
			// de
			// datos las serializamos y las enviamos
			ListFriendRequest listFriendRequest = new ListFriendRequest();

			/*
			 * una vez recuperadas las peticiones las modelamos, usando
			 * RequestModel, para prepar la respuesta, de modo que solo enviemos
			 * los datos necesarios
			 */
			listFriendRequest.list = new ArrayList<ListFriendRequest.RequestModel>();
			if (tmpFriendReq != null) {

				logger.info("******** we Found " + tmpFriendReq.size()
						+ " Friend Request ********");
				// int i = 0;
				try {
					for (FriendRequest frReq : tmpFriendReq) {

						listFriendRequest.list
								.add((RequestModel) new ListFriendRequest.RequestModel(

								frReq.getId(), frReq.getUsersByIdUserSrc()
										.getIdImg(), frReq
										.getUsersByIdUserSrc().getId(), frReq
										.getUsersByIdUserSrc().getName(), frReq
										.getUsersByIdUserTarget().getId(),
										frReq.getUsersByIdUserTarget()
												.getName(), frReq.getStatus()));

					}

				} catch (Exception e) {
					// caso de error:
					// notificamos de la causa del error al cliente
					final String errorMsg = "******** ERROR, se han encontrado "
							+ tmpFriendReq.size()
							+ " peticiones de amitad para el usuario "
							+ userId
							+ ", pero se ha producido un error generando la respuesta de las peticiones de amistad ********";
					logger.severe(errorMsg);
					errorJsonObj = new StatusPackage(
							StatusPackage.STATUS_ERROR, "Wrong request")
							.serializeToJson();

					throw new Exception(errorJsonObj, e);

				}
			} else {
				logger.info("******* we Didn't Found Any Friend Request ********");
			}

			String listFriendRequestJson = listFriendRequest.serializeToJson();
			logger.info("RequestModel " + listFriendRequestJson);
			// devolvemos el resultado
			out.print(listFriendRequestJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession(); // _cerramos la sesion_
		}
	}

	 

	/**
	 * consulta las canciones de un usuario parametros: (id_user | id_favoritos)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public List<Canciones> processFindSongs(HttpServletRequest request,
			HttpServletResponse response) {
		final SongsDAO songsDAO = new SongsDAO();
		logger.info("processFindUser begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();
		ListSongs listSongs = new ListSongs();
		PrintWriter out = null;

		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}

			/*
			 * buscamos las canciones usando el idusuario ó el el identificador
			 * de los favoritos, si idUsuario = null
			 */

			Long idUser = null;
			String favoritesIdentification = null;
			try {
				// cogemos los parametros de la peticion
				idUser = Long.valueOf(request.getParameter(HttpArgs.idUser));
			} catch (Exception e) {
				// e.printStackTrace();
			}

			try {
				favoritesIdentification = request
						.getParameter(HttpArgs.favoritesIdentification);
			} catch (Exception e) {
				out.print(errorJsonObj);
				e.printStackTrace();
			}

			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_
			songsDAO.setSession(songsDAO.getDAOManager());

			// si no se especifica ninguna lista, buscamos todas las canciones
			// del usuario.
			if (favoritesIdentification == null) {
				logger.info("**********  find songs by id_user  **********");
				listSongs.list = songsDAO.getSongsByUser(idUser);
			} else if (favoritesIdentification != null) {
				try {
					logger.info("******** find songs by id_favorite **********");
					FavoritesDAO favoritesDAO = new FavoritesDAO();
					favoritesDAO.setSession(songsDAO.getSession());
					// recuperamos el objeto favoritos de la base de datos y
					// mostramos sus canciones
					Favorites favorites = favoritesDAO
							.getFavoritesByIdentification(
									favoritesIdentification).get(0);

					listSongs.list = new ArrayList<Canciones>(
							favorites.getCanciones());
					Collections.sort(listSongs.list);

				} catch (Exception e) {
					e.printStackTrace();
					out.print(errorJsonObj);
					return null;
				}

			} else {
				logger.info("ERROR not valid params");
				out.print(errorJsonObj);
				return null;
			}

			// caso de error:
			if (listSongs.list == null) {
				out.print(errorJsonObj);
				logger.info("processFindUser... no songs found !");
				return null;
			}

			// serializamos el la lista de canciones en formato Json
			String listSongsJson = listSongs.serializeToJson();

			// devolvemos el resultado
			out.print(listSongsJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			songsDAO.closeSession(); // _cerramos la sesion_
		}
		return listSongs.list;
	}

	public void processFindFavoritesFromUser(HttpServletRequest request,
			HttpServletResponse response) {
		// final SongsDAO songsDAO = new SongsDAO();
		final UsersDAO usersDAO = new UsersDAO();
		ListFavorites listFavorites = null;
		logger.info("processFindFavoritesFromUser begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();
		PrintWriter out = null;

		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			Long idUser = null;
			// Long idFavorites = null;
			try {
				// cogemos los parametros de la peticion
				idUser = Long.valueOf(request.getParameter(HttpArgs.idUser));
			} catch (Exception e) {
				// e.printStackTrace();
			}

			System.out.println("idUser = " + idUser);
			// creamos un objeto de la clase ListUsers a partir del resultado de
			// la consulta para serializaro
			// _open the session before_

			if (idUser != null) {
				logger.info("find songs by id_user");
				// listSongs.list = songsDAO.getSongsByUser(idUser);
				usersDAO.setSession(usersDAO.getDAOManager());
				Users user = usersDAO.getUsersById(idUser).get(0);
				listFavorites = new ListFavorites();

				System.out.println("user.getFavoriteses() = "
						+ user.getFavoriteses().size());
				listFavorites.list = new ArrayList<Favorites>(
						user.getFavoriteses());
				Collections.sort(listFavorites.list);

			} else {
				logger.info("ERROR not valid params");
				out.print(errorJsonObj);
				return;
			}

			// caso de error:
			if (listFavorites.list == null) {
				out.print(errorJsonObj);
				logger.info("processFindFavoritesFromUser... no favorites found !");
				return;
			}

			// serializamos el la lista de canciones en formato Json
			String listSongsJson = listFavorites.serializeToJson();

			// devolvemos el resultado
			out.print(listSongsJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
			// songsDAO.closeSession(); // _cerramos la sesion_
		}
	}

	/**
	 * params: favoritesIdentification, si no se encuentra ninguna con ese
	 * identificador la crea (usamos el nombre para identificarla en lugar del
	 * id, porque al poderse crear listas desconectado de internet, no tienen
	 * porque coincidir param: favoritesName, idUser, favoritesNewName (only on
	 * update)
	 * 
	 * @param request
	 * @param response
	 */
	public void processAddOrUpdateFavorite(HttpServletRequest request,
			HttpServletResponse response) {

		logger.info("processAddOrUpdateFavorite begin...");
		// cogemos los parametros de la peticion
		final UsersDAO usersDAO = new UsersDAO();
		final FavoritesDAO favoritesDAO = new FavoritesDAO();

		Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();

			String favoritesIdentification = request
					.getParameter(HttpArgs.favoritesIdentification);
			System.out.println("favoritesIdentificaton ");
			String favoritesNewName = request
					.getParameter(HttpArgs.favoritesNewName);
			Long idUser = Long.parseLong(request.getParameter(HttpArgs.idUser));

			usersDAO.setSession(usersDAO.getDAOManager());
			user = usersDAO.getUsersById(idUser).get(0);
			/*
			 * NOTE: in order to advoid an
			 * "org.hibernate.PersistentObjectException: detached entity passed
			 * to persist: Users" _the same session must to be used_
			 */
			favoritesDAO.setSession(usersDAO.getSession());
			Favorites favorites = null;

			try {
				/*
				 * si existe ya la lista solo la actualizamos su nombre, pero si
				 * no existe la creamos
				 */

				List<Favorites> favoritesFromDB = favoritesDAO
						.getFavoritesByIdentificationAndUserId(
								favoritesIdentification, idUser);

				if (favoritesFromDB == null || favoritesFromDB.isEmpty()) {
					favoritesFromDB = favoritesDAO.getFavoritesByNameAndIdUser(
							favoritesNewName, idUser);
				}

				favorites = favoritesFromDB.get(0);

			} catch (Exception e) {
				// e.printStackTrace();
			}

			if (favorites == null) {
				logger.info("creating new favorites list " + favoritesNewName
						+ " identification = " + favoritesIdentification);
				// creamos la lista de favoritos
				favorites = new Favorites(user, favoritesIdentification,
						favoritesNewName);
				favoritesDAO.save(favorites);
			} else {
				logger.info("updating new favorites list " + favoritesNewName
						+ " identification = " + favoritesIdentification);
				favorites.setNameOfList(favoritesNewName);
				favoritesDAO.update(favorites);
			}

			// en caso de exito, notificamos al cliente
			logger.info("processAddOrUpdateFavorite() created user " + user);
			String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS, null).serializeToJson();
			out.print(successJson);
		} catch (Exception e) {
			logger.info("processAddOrUpdateFavorite() -ERROR -> Exception");
			e.printStackTrace();
			String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
					"Error Updating the Favorites List").serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
	}

	/**
	 * crea los datos de prueba en la base de datos
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public List<Users> processCreateTestData(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processCreateTestData begin...");

		ListUsers listUsers = new ListUsers();

		PrintWriter out = null;

		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}
			// CREAMOS LOS USUARIOS
			listUsers.list = new MockDAO().createTestData();

			if (listUsers != null) {
				String successJsonObj = new StatusPackage(
						StatusPackage.STATUS_SUCCESS,
						"Se han creado los datos de prueba").serializeToJson();
				out.print(successJsonObj);
			}
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			// notificamos del error
			e.printStackTrace();
			final String errorJsonObj = new StatusPackage(
					StatusPackage.STATUS_ERROR, "error creating test data ")
					.serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
		return listUsers.list;
	}

	public Users processRemoveFriend(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processRemoveFriend begin...");
		Users userSelf = null;
		PrintWriter out = null;
		String errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
				"Wrong request").serializeToJson();

		// Logger.info("removing friend "+request
		// .getParameter(HttpArgs.idUserFriend + " from: ");
		Long idUserFriend = Long.parseLong(request
				.getParameter(HttpArgs.idUserFriend));
		Long idUserSelf = Long.parseLong(request
				.getParameter(HttpArgs.idUserOwner));

		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}
			usersDAO.setSession(usersDAO.getDAOManager());
			userSelf = usersDAO.getUsersById(idUserSelf).get(0);
			Users userFriend = usersDAO.getUsersById(idUserFriend).get(0);

			userSelf.getFriends().remove(userFriend);
			userFriend.getFriends().remove(userSelf);
			usersDAO.update(userFriend);

			// usersDAO.setSession(usersDAO.getDAOManager());
			// usersDAO.update(userSelf);

			ListUsers listUsers = new ListUsers();
			listUsers.list = usersDAO.getUsersByFriendId(idUserSelf);

			// caso de error
			if (listUsers == null) {
				out.print(errorJsonObj);
			}

			String returnJson = listUsers.serializeToJson();
			logger.info(returnJson);

			// final String succesJson = new StatusPackage(
			// StatusPackage.STATUS_SUCCESS, "the user has been added")
			// .serializeToJson();
			out.print(returnJson);
			response.setStatus(HttpServletResponse.SC_OK);

		} catch (Exception e) {
			// notificamos del error
			e.printStackTrace();
			errorJsonObj = new StatusPackage(StatusPackage.STATUS_ERROR,
					"error creating test data ").serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
		return userSelf;
	}

	public Users processAddFriend(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processAddFriend begin...");
		Users userOwner = null;
		PrintWriter out = null;

		String idUserFriend = request.getParameter(HttpArgs.idUserFriend);
		String idUserOwner = request.getParameter(HttpArgs.idUserOwner);

		logger.info("idUserFriend =" + idUserFriend);
		logger.info("idUserOwner =" + idUserOwner);

		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}
			usersDAO.setSession(usersDAO.getDAOManager());
			userOwner = usersDAO.getUsersById(Long.parseLong(idUserOwner)).get(
					0);
			Users userFriend = usersDAO.getUsersById(
					Long.parseLong(idUserFriend)).get(0);

			userOwner.getFriends().add(userFriend);
			userFriend.getFriends().add(userOwner);
			usersDAO.save(userFriend);

			System.out.println("userseft.getFrinds"
					+ userOwner.getFriends().size());
			System.out.println("userseft.getFrinds"
					+ userFriend.getFriends().size());

			/**
			 * borramos las peticiones realacionadas...
			 */
			// if (requestSend == false) {
			FriendRequestDAO fRequestDAO = new FriendRequestDAO();
			fRequestDAO.setSession(usersDAO.getSession());
			// fRequestDAO.getSession().clear();
			// fRequestDAO.getSession().setCacheMode(CacheMode.IGNORE);
			List<FriendRequest> requestFromDb = fRequestDAO.getRequests(
					Long.parseLong(idUserOwner), Long.parseLong(idUserFriend));
			logger.info(" processRequestFriend() requestFromDb = "
					+ requestFromDb);

			if (requestFromDb == null || requestFromDb.isEmpty()) {
				logger.info("retrieving friend request from cache....");

				fRequestDAO.getSession().setCacheMode(CacheMode.NORMAL);
				requestFromDb = fRequestDAO.getRequests(
						Long.parseLong(idUserOwner),
						Long.parseLong(idUserFriend));
			}

			if (requestFromDb != null && requestFromDb.isEmpty() == false) {

				logger.info("***********************  elimando las peticiones correspondientes ....  *********************** "
						+ requestFromDb.size());

				for (FriendRequest friendRequest : requestFromDb) {
					logger.info(" elimiando peticion .... " + friendRequest);
					fRequestDAO.delete(friendRequest);
				}

			} else {
				logger.info("***********************  no hay peticiones que eliminar ....  *********************** ");
			}

			logger.info("***********************  resulting friend request to userId: "
					+ idUserOwner + " ............  *********************** ");
			// fRequestDAO.getSession().clear();
			List<FriendRequest> resultingFR = fRequestDAO
					.getRequestByUserTarget(Long.parseLong(idUserOwner),
							RequestModel.REQUEST);
			if (resultingFR != null) {
				for (FriendRequest friendRequest : resultingFR) {
					logger.info(" > friendRequest:  " + friendRequest);
				}
			}

			// throw new Exception();

			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS, "the user has been added")
					.serializeToJson();

			out.print(successJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			// notificamos del error
			e.printStackTrace();
			final String errorJsonObj = new StatusPackage(
					StatusPackage.STATUS_ERROR, "error creating test data ")
					.serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
		return userOwner;
	}

	public Users processDismisFriendRequest(HttpServletRequest request,
			HttpServletResponse response) {
		final UsersDAO usersDAO = new UsersDAO();
		logger.info("processAddFriend begin...");
		Users userOwner = null;
		PrintWriter out = null;

		String idUserFriend = request.getParameter(HttpArgs.idUserFriend);
		String idUserOwner = request.getParameter(HttpArgs.idUserOwner);

		logger.info("idUserFriend =" + idUserFriend);
		logger.info("idUserOwner =" + idUserOwner);

		try {
			out = response.getWriter();
			if (out == null) {
				return null;
			}
			Session session = usersDAO.getDAOManager();
			usersDAO.setSession(session);
			userOwner = usersDAO.getUsersById(Long.parseLong(idUserOwner)).get(
					0);
			// Users userFriend = usersDAO.getUsersById(
			// Long.parseLong(idUserFriend)).get(0);

			/**
			 * borramos las peticiones realacionadas...
			 */
			// if (requestSend == false) {
			FriendRequestDAO fRequestDAO = new FriendRequestDAO();
			fRequestDAO.setSession(usersDAO.getSession());
			// fRequestDAO.getSession().clear();
			// fRequestDAO.getSession().setCacheMode(CacheMode.IGNORE);
			List<FriendRequest> requestFromDb = fRequestDAO.getRequests(
					Long.parseLong(idUserOwner), Long.parseLong(idUserFriend));
			logger.info(" processRequestFriend() requestFromDb = "
					+ requestFromDb);

			if (requestFromDb == null || requestFromDb.isEmpty()) {
				logger.info("retrieving friend request from cache....");

				fRequestDAO.getSession().setCacheMode(CacheMode.NORMAL);
				requestFromDb = fRequestDAO.getRequests(
						Long.parseLong(idUserOwner),
						Long.parseLong(idUserFriend));
			}

			if (requestFromDb != null && requestFromDb.isEmpty() == false) {

				logger.info("***********************  elimando las peticiones correspondientes ....  *********************** "
						+ requestFromDb.size());

				for (FriendRequest friendRequest : requestFromDb) {
					logger.info(" elimiando peticion .... " + friendRequest);
					fRequestDAO.delete(friendRequest);
				}

			} else {
				logger.info("***********************  no hay peticiones que eliminar ....  *********************** ");
			}

			logger.info("***********************  resulting friend request to userId: "
					+ idUserOwner + " ............  *********************** ");
			// fRequestDAO.getSession().clear();
			List<FriendRequest> resultingFR = fRequestDAO
					.getRequestByUserTarget(Long.parseLong(idUserOwner),
							RequestModel.REQUEST);
			if (resultingFR != null) {
				for (FriendRequest friendRequest : resultingFR) {
					logger.info(" > friendRequest:  " + friendRequest);
				}
			}

			// throw new Exception();

			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS, "the user has been added")
					.serializeToJson();

			out.print(successJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			// notificamos del error
			e.printStackTrace();
			final String errorJsonObj = new StatusPackage(
					StatusPackage.STATUS_ERROR, "error creating test data ")
					.serializeToJson();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			usersDAO.closeSession();
		}
		return userOwner;
	}

	public void processUpdateSong(HttpServletRequest request,
			HttpServletResponse response) {

		final SongsDAO songsDAO = new SongsDAO();
		logger.info("processUpdateSong begin...");

		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// cogemos los parametros de la peticion
		// Users user = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			String srcPath = request.getParameter(HttpArgs.songSrcPath);
			Long idUser = Long.parseLong(request.getParameter(HttpArgs.idUser));
			String songName = request.getParameter(HttpArgs.songName);
			String songAuthor = request.getParameter(HttpArgs.songAuthor);

			// recuperamos la cancion de la base de datos y lo actualizamos:
			songsDAO.setSession(songsDAO.getDAOManager());
			List<Canciones> songsFound = songsDAO.getSongBySrcPathAdnUserId(
					srcPath, idUser);

			// caso de error:
			if (songsFound == null || songsFound.isEmpty()) {
				out.print(errorJsonObj);
				return;
			}

			Canciones song = songsFound.get(0);

			// actualizamos los atributos de la cancion (el src_path no se puede
			// modificar)
			song.setSongAuthor(songAuthor);
			song.setSongName(songName);

			logger.info("song to update  " + song);
			// actualizamos la cancion en la base de datos
			songsDAO.update(song);

			// logging
			String songJson = song.serializeToJson();
			out.print(songJson);
			logger.info("song updated: \n" + songJson);
			logger.info("processUpdateSong() -OK");

		} catch (ConstraintViolationException e) {
			logger.info("processLogin() -ERROR -> ConstraintViolationException");
			e.printStackTrace();
			String errorJsonObjTmp = new StatusPackage(
					StatusPackage.STATUS_ERROR,
					"Error performing Sign Up - the name or the author of the songs are invalid")
					.serializeToJson();
			out.print(errorJsonObjTmp);
		} catch (Exception e) {
			logger.info("processLogin() -> Exception");
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			songsDAO.closeSession();
		}
	}

	/**
	 * 
	 * Agrega una cancion a una lista de favoritos, si la cancion no existe en
	 * la base de datos, la crea y si la lista de favoritos no exite, la crea;
	 * pero si ya si existe la lista (con el mismo favoritesIdentification) y
	 * con un nombre diferente, actualiza el nombre de la lista.
	 * 
	 * @param request
	 * @param response
	 */
	public void processAddSongToFavorite(HttpServletRequest request,
			HttpServletResponse response) {

		final SongsDAO songsDAO = new SongsDAO();
		final FavoritesDAO favoritesDAO = new FavoritesDAO();
		Favorites favorites = null;
		logger.info("processAddSongToFavorite begin...");

		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// cogemos los parametros de la peticion
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			Long idUser = Long.parseLong(request.getParameter(HttpArgs.idUser));
			String favoritesIdentification = request
					.getParameter(HttpArgs.favoritesIdentification);
			String favoritesName = request.getParameter(HttpArgs.favoritesName);
			String songSrcPath = request.getParameter(HttpArgs.songSrcPath);
			String songPhotoEncoded = request
					.getParameter(HttpArgs.songPhotoEncoded);
			String songName = request.getParameter(HttpArgs.songName);
			String songAuthor = request.getParameter(HttpArgs.songAuthor);



			favoritesDAO.setSession(favoritesDAO.getDAOManager());
			favoritesDAO.getSession().beginTransaction();
			// buscamos la lista de favoritos
			// boolean newFavorites = false;
			try {
				favorites = favoritesDAO.getFavoritesByIdentification(
						favoritesIdentification).get(0);
				// actualizamos el nombre de la lista (por si ha el usuario lo
				// ha cambiado mientras no estaba conectado)
				favorites.setNameOfList(favoritesName);
				// favoritesDAO.update(favorites);
			} catch (Exception e) {
				// si no existe la lista de favoritos la creamos
				logger.info("creando una lista de favoritos nueva en el servidor  favoritesIdentification: "
						+ favoritesIdentification);
				// buscamos el usuario en la base de datos
				UsersDAO usersDAO = new UsersDAO();
				usersDAO.setSession(favoritesDAO.getSession());
				try {
					Users user = usersDAO.getUsersById(idUser).get(0);

					// Users users, String identification, String nameOfList
					favorites = new Favorites(user, favoritesIdentification,
							favoritesName);

				} catch (Exception ex) {
					logger.info("error creando una nueva lista favoritos,  el usuario: "
							+ idUser + " no existe en la base de datos");
					throw ex;
				}
			}


			// recuperamos la cancion de la base de datos
			songsDAO.setSession(favoritesDAO.getSession());
			List<Canciones> songsFound = songsDAO.getSongBySrcPathAdnUserId(
					songSrcPath, idUser);

			Canciones song = null;
			if (songsFound == null || songsFound.isEmpty()) {
				// si no existe la cancion la creamos
				logger.info("creating new song");

				song = new Canciones(songName, songAuthor, songSrcPath,
						songPhotoEncoded);


			} else {
				logger.info("retrieving the song from db");
				song = songsFound.get(0);
			}

			if (favorites.getCanciones() != null
					&& favorites.getCanciones().contains(song)) {
				final String errorJson = new StatusPackage(
						StatusPackage.STATUS_ERROR,
						"There can't be duplicated songs on the same favorites list")
						.serializeToJson();
				out.print(errorJson);
				return;
			}

			logger.info("favorites to update  " + favorites);
			// actualizamos la lista de favoritos y guardamos

			logger.info("new song is been added favorites  song: getPhotoEncoded:\n"
					+ song.getPhotoEncoded() + "\n----------------------------");

			// logger.info("new song is been added favorites "
			// + favoritesIdentification + " song: name:"
			// + song.getSongName() + "id: " + song.getIdSong()
			// + "\n----------------------------");

			if (favorites.getCanciones() == null) {
				favorites.setCanciones(new HashSet<Canciones>());
			}
			if (song.getFavorites() == null) {
				song.setFavorites(new HashSet<Favorites>());
			}
			song.getFavorites().add(favorites);
			songsDAO.getSession().saveOrUpdate(song);
			favorites.getCanciones().add(song);
			favoritesDAO.getSession().saveOrUpdate(favorites);
			favoritesDAO.getSession().getTransaction().commit();
			// favoritesDAO.saveOrUpdate(favorites);

			// logging
			// String songJson = song.serializeToJson();
			// out.print(songJson);
			// logger.info("song updated: \n" + songJson);
			logger.info("processUpdateSong() -OK");

			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS,
					"The favorites list is updated").serializeToJson();
			out.print(successJson);

			songsFound = songsDAO
					.getSongBySrcPathAdnUserId(songSrcPath, idUser);
		} catch (ConstraintViolationException e) {
			logger.info("processAddSongToFavorite() -ERROR -> ConstraintViolationException");
			e.printStackTrace();
			String errorJsonObjTmp = new StatusPackage(
					StatusPackage.STATUS_ERROR,
					"Error performing Sign Up - the name or the author of the songs are invalid")
					.serializeToJson();
			out.print(errorJsonObjTmp);
			favoritesDAO.getSession().getTransaction().rollback();// //////
		} catch (Exception e) {
			favoritesDAO.getSession().getTransaction().rollback();// //////
			logger.info("processAddSongToFavorite() -> Exception");
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			// favoritesDAO.closeSession();
		}
	}

	public void cleanFavoritesForUser(Long idUser) {

		final SongsDAO songsDAO = new SongsDAO();
		final FavoritesDAO favoritesDAO = new FavoritesDAO();
		Canciones song = null;
		Favorites favorites = null;
		logger.info("cleanFavoritesForUser begin...");

		try {
			favoritesDAO.setSession(favoritesDAO.getDAOManager());
			songsDAO.setSession(favoritesDAO.getSession());

			List<Canciones> songsFound = songsDAO.getSongsByUser(idUser);
			final int songsFoundSize = songsFound.size();

			// for each song if if repeated sustitude

			HashMap<String, List<Canciones>> songsMap = new HashMap<String, List<Canciones>>();

			for (Canciones songN : songsFound) {
				List<Canciones> songs = songsMap.get(songN.getSrcPath());
				if (songs == null) {
					songs = new ArrayList<Canciones>();
				}
				songs.add(songN);
				songsMap.put(songN.getSrcPath(), songs);
			}
			System.out.println("fixing songs begin...  ");
			Set<String> songsMapKeys = songsMap.keySet();
			for (String key : songsMapKeys) {
				List<Canciones> cancionesN = songsMap.get(key);
				if (cancionesN != null || cancionesN.size() > 1) {
					final Canciones CancionFirst = cancionesN.get(0);
					final int size = cancionesN.size();
					for (int i = 1; i < size; i++) {
						System.out.println("fixing song... 1: " + cancionesN);
						Canciones cancionNM = cancionesN.get(i);
						Hibernate.initialize(cancionNM.getFavorites());
						if (cancionNM.getFavorites() == null) {
							logger.info("songsDAO.delete(cancionNM) cancionNM -> "
									+ cancionNM);
							songsDAO.delete(cancionNM);
						}
						logger.info("	Hibernate.initialize(cancionNM.getFavorites());");
						Set<Favorites> favoritesN = cancionNM.getFavorites();
						if (favoritesN != null) {
							for (Favorites favoritesNM : favoritesN) {
								System.out.println("fixing song... 2: "
										+ favoritesNM);
								favoritesNM.getCanciones().remove(cancionNM);
								favoritesNM.getCanciones().add(CancionFirst);
								favoritesDAO.update(favorites);
							}
						} else {
							logger.warning("the song doesn't have favorites");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void processRemoveSongFromFavorite(HttpServletRequest request,
			HttpServletResponse response) {
		Canciones song = null;
		Favorites favorites = null;
		final SongsDAO songsDAO = new SongsDAO();
		final FavoritesDAO favoritesDAO = new FavoritesDAO();
		logger.info("processRemoveSongFromFavorite begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// ListSongs listSongs = new ListSongs();
		PrintWriter out = null;

		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			Long idUser = null;
			String songSrcPath = null;
			String favoritesIdentification = null;
			String favoritesName = null;
			try {
				// cogemos los parametros de la peticion
				idUser = Long.parseLong(request.getParameter(HttpArgs.idUser));
				songSrcPath = request.getParameter(HttpArgs.songSrcPath);
				favoritesIdentification = request
						.getParameter(HttpArgs.favoritesIdentification);
				favoritesName = request.getParameter(HttpArgs.favoritesName);
			} catch (Exception e) {
				e.printStackTrace();
				out.print(errorJsonObj);
				return;
			}

			// opens the session

			favoritesDAO.setSession(songsDAO.getDAOManager());
			songsDAO.setSession(favoritesDAO.getSession());

			// buscamos la lista de favoritos
			boolean isNewFavorites = false;
			try {
				favorites = favoritesDAO.getFavoritesByIdentification(
						favoritesIdentification).get(0);
				// actualizamos el nombre de la lista (por si ha el usuario lo
				// ha cambiado mientras no estaba conectado)
				if (favoritesName != null
						&& favoritesName.trim().isEmpty() == false) {
					favorites.setNameOfList(favoritesName);
				}

			} catch (Exception e) {
				isNewFavorites = true;
				// si no existe la lista de favoritos la creamos
				logger.info("creando una lista de favoritos nueva en el servidor  favoritesIdentification: "
						+ favoritesIdentification);
				// buscamos el usuario en la base de datos
				UsersDAO usersDAO = new UsersDAO();
				usersDAO.setSession(favoritesDAO.getSession());
				try {
					Users user = null;
					usersDAO.getUsersById(idUser).get(0);

					favorites = new Favorites(user, favoritesIdentification,
							favoritesName);

				} catch (Exception ex) {
					logger.info("ERROR creando una nueva lista favoritos,  el usuario: "
							+ idUser + " no existe en la base de datos");
					throw ex;
				}

			}

			// busca la cancion si no encuentra no hace nada
			try {
				song = songsDAO.getSongBySrcPathAdnUserId(songSrcPath, idUser)
						.get(0);

			} catch (Exception e) {
				// caso de exito:
				// la cancion no se tiene que eliminar porque no
				// existe en la basde de datos del servidor.
				final String successJson = new StatusPackage(
						StatusPackage.STATUS_SUCCESS,
						"The song of the user it isn't on database")
						.serializeToJson();
				out.print(successJson);
				return;
			}
			Hibernate.initialize(song.getFavorites());

			// si la cancion existe en la base de datos del servidor:
			if (favorites.getCanciones() != null) {
				logger.info("deleting songs from favorite");
				favorites.getCanciones().remove(song);
			}
			if (song.getFavorites() != null) {
				logger.info("deleting favorite from song");
				song.getFavorites().remove(favorites);
			}
			// try {
			// songsDAO.update(song);
			// } catch (Exception e) {
			// e.printStackTrace();
			// }

			logger.info("checking if the Song is orphan...");
			final int songInNfavs = songsDAO.getFavoritesFromSongAndUser(
					song.getSrcPath(), idUser);

			logger.info("songInNfavs = " + songInNfavs);
			// final int songInNfavs = 1;
			if (songInNfavs == 1) {
				logger.info("THE SONG IS ORPHAN !");
				song = (Canciones) songsDAO.getSession().merge(song);
				try {
					songsDAO.delete(song);
					// if the song don't exist throws and exception
				} catch (Exception e) {
					// do nothing
				}
			}
			// actualizamos la lista de favoritos modificada
			logger.info("song removed from favorites list updating favorite list: "
					+ favorites);
			favoritesDAO.saveOrUpdate(favorites);
			logger.info("processRemoveSongFromFavorite() -OK");

			// notificamos al cliente
			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS,
					"The song has been removed from favorites")
					.serializeToJson();
			out.print(successJson);

		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			try {
				favoritesDAO.closeSession(); // _cerramos la sesion_
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
		return;

	}

	public void processRemoveFavorite(HttpServletRequest request,
			HttpServletResponse response) {

		// Canciones song = null;
		// Favorites favorites = null;
		final SongsDAO songsDAO = new SongsDAO();
		final FavoritesDAO favoritesDAO = new FavoritesDAO();
		logger.info("processRemoveFavorite begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// ListSongs listSongs = new ListSongs();
		PrintWriter out = null;
		favoritesDAO.setSession(favoritesDAO.getDAOManager());
		songsDAO.setSession(favoritesDAO.getSession());

		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			String favoritesIdentification = request
					.getParameter(HttpArgs.favoritesIdentification);

			Favorites favorites = null;
			try {
				favorites = favoritesDAO.getFavoritesByIdentification(
						favoritesIdentification).get(0);

			} catch (Exception e) {
				final String successJson = new StatusPackage(
						StatusPackage.STATUS_SUCCESS,
						"The favorite list it isn't on database")
						.serializeToJson();
				out.print(successJson);
				return;
			}

			List<Canciones> favSongsFromDb = songsDAO
					.getSongsByFavoriteFavoriteIdentification(favoritesIdentification);

			// logger.info("canciones favSongsFromDb:");
			for (Canciones canciones : favSongsFromDb) {
				logger.info(" " + canciones);
			}

			if (favSongsFromDb != null && favSongsFromDb.isEmpty() == false) {
				for (Canciones songFDBN : favSongsFromDb) {
					logger.info("checking if the Song is orphan...");
					final int songInNfavs = songsDAO
							.getFavoritesFromSongAndUser(songFDBN.getSrcPath(),
									favorites.getUsers().getId());

					logger.info("songInNfavs = " + songInNfavs);
					// final int songInNfavs = 1;
					if (songInNfavs == 1) {
						logger.info("THE SONG IS ORPHAN !");
						songFDBN = (Canciones) songsDAO.getSession().merge(
								songFDBN);
						try {
							songsDAO.delete(songFDBN);
							// if the song don't exist throws and exception
						} catch (Exception e) {
							// do nothing
						}
					}
				}
			}

			if (favorites != null) {
				Hibernate.initialize(favorites.getCanciones());
				Hibernate.initialize(favorites.getUsers());
				favoritesDAO.getSession().delete(favorites);
				favoritesDAO.delete(favorites);
			}


			logger.info("processRemoveFavorite() -OK");

			// notificamos al cliente
			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS,
					"the favorites list has been removed on the server")
					.serializeToJson();
			out.print(successJson);

		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			favoritesDAO.closeSession(); // _cerramos la sesion_
		}
		return;
	}

	// }
	public void processUpdateFavoriteAndItsSongs(HttpServletRequest request,
			HttpServletResponse response) {

		// Canciones song = null;
		// Favorites favorites = null;
		Favorites favoriteFound = null;
		final SongsDAO songsDAO = new SongsDAO();
		final UsersDAO usersDAO = new UsersDAO();
		final FavoritesDAO favoritesDAO = new FavoritesDAO();
		logger.info("processUpdateFavoriteAndItsSongs begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// ListSongs listSongs = new ListSongs();
		PrintWriter out = null;

		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			favoritesDAO.setSession(favoritesDAO.getDAOManager());
			songsDAO.setSession(favoritesDAO.getSession());
			usersDAO.setSession(favoritesDAO.getSession());

			String favoriteAndItsSongs = request
					.getParameter(HttpArgs.favoriteAndItsSongs);

			logger.info("param favoriteAndItsSongs: " + favoriteAndItsSongs);
			FavoriteAndSongs favAndSongs = (FavoriteAndSongs) new FavoriteAndSongs()
					.deSerializeFromJson(favoriteAndItsSongs);

			Favorites favoriteFromRequest = favAndSongs.favorites;
 

			logger.info("buscamos el usuario de la lista ");
			// buscamos el usuario de la lista
			final Long idUser = favAndSongs.idUser;
			Users user = null;
			try {
				logger.info("buscando el usuario: " + idUser);
				// user = usersDAO.getUsersById(idUser).get(0);
				List<Users> users = usersDAO.getUsersById(idUser);
				user = users.get(0);

			} catch (Exception e) {
				// caso de error: e usuario no existe
				logger.info("El usuario no existe id:" + idUser);
				e.printStackTrace();
				final String errorJson = new StatusPackage(
						StatusPackage.STATUS_ERROR, "The user " + idUser
								+ " doesn't exist!").serializeToJson();
				out.print(errorJson);
				return;
			}

			logger.info("agregamos el usuario ");
			favoriteFromRequest.setUsers(user);

			final String favoritesIdentification = favAndSongs.favorites
					.getIdentification();

			 
			 
			/*
			 * para evitar canciones huerfanas, limpiamos la lista de favoritos
			 * y la volvemos a generar
			 */
			List<Canciones> favSongsFromDb = songsDAO
					.getSongsByFavoriteFavoriteIdentification(favoriteFromRequest
							.getIdentification());

			logger.info("canciones favSongsFromDb:");
			for (Canciones canciones : favSongsFromDb) {
				logger.info(" " + canciones);
			}
 

			boolean newFavorite = false;
			try {
				favoriteFound = favoritesDAO.getFavoritesByIdentification(
						favoritesIdentification).get(0);
				Hibernate.initialize(favoriteFound.getCanciones());
				logger.info("favoritefound = " + favoriteFound);
			 
				favoriteFromRequest.setNameOfList(favoriteFromRequest
						.getNameOfList());

			} catch (Exception e) {
				newFavorite = true;
				// si no existe la lista de favoritos usamos la que recibimos
				favoriteFound = favoriteFromRequest;
				logger.info("creando una lista de favoritos nueva en el servidor  favoritesIdentification: "
						+ favoritesIdentification);
			}

			if (favoriteFound.getCanciones() == null) {
				favoriteFound.setCanciones(new HashSet<Canciones>());
			}
			for (Canciones songN : favAndSongs.songs) {
				try {
					Canciones songNFoundDB = songsDAO
							.getSongBySrcPathAdnUserId(songN.getSrcPath(),
									favAndSongs.idUser).get(0);

					favoriteFound.getCanciones().add(songNFoundDB);
				} catch (Exception e) {
					songsDAO.getSession().save(songN);
					favoriteFound.getCanciones().add(songN);
				}
			}

			if (newFavorite == true) {
				logger.info("saving new Favorite");
				favoritesDAO.save(favoriteFound);
			} else {
				songsDAO.getSession().flush();
				logger.info("updating a Favorite");
				try {
					favoritesDAO.update(favoriteFound);
				} catch (ConstraintViolationException e) {
					favoritesDAO.merge(favoriteFound);
				}

			}

			logger.info("processUpdateFavoriteAndItsSongs() -OK");

			// notificamos al cliente
			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS,
					"the favorites list has been updated on the server")
					.serializeToJson();
			out.print(successJson);

		} catch (Exception e) {
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			favoritesDAO.closeSession(); // _cerramos la sesion_
		}
		return;
	}

	public void processCommitFavorites(HttpServletRequest request,
			HttpServletResponse response) {

 
		final FavoritesDAO favoritesDAO = new FavoritesDAO();
		final SongsDAO songsDAO = new SongsDAO();
		logger.info("processCommitFavorites begin...");
		final String errorJsonObj = new StatusPackage(
				StatusPackage.STATUS_ERROR, "Wrong request").serializeToJson();

		// ListSongs listSongs = new ListSongs();
		PrintWriter out = null;
		org.hibernate.Transaction transaction = null;
		try {
			out = response.getWriter();
			if (out == null) {
				return;
			}

			// usersDAO.setSession(favoritesDAO.getSession());
			Session session = favoritesDAO.getDAOManager();
			favoritesDAO.setSession(session);
			songsDAO.setSession(session);

			String favsListStr = request.getParameter(HttpArgs.favoritesList);
			Long idUser = Long.valueOf(request.getParameter(HttpArgs.idUser));

			logger.info("param favoriteAndItsSongs: " + favsListStr);

			ListFavorites listFavorites = (ListFavorites) new ListFavorites()
					.deSerializeFromJson(favsListStr);

			List<Favorites> favoritesFrRequest = listFavorites.list;

			 
			// Begin Transaction to update favorites
			transaction = session.beginTransaction();

			logger.info(" ......................  "
					+ " eliminando todos los favoritos del servidor del usuario "
					+ idUser
					+ " .....................................................");


			List<Favorites> favsFrServ = favoritesDAO
					.getFavoritesByIdUserWithItsSongs(idUser);

			if (favsFrServ != null) {
				for (Favorites favFrServN : favsFrServ) {
					logger.info(" ......................  "
							+ "  elminando lista " + favFrServN.getNameOfList());
					for (Canciones cancionOnFav : favFrServN.getCanciones()) {
						try {

							session.delete(cancionOnFav);
						} catch (Exception e) {
							logger.severe(" ****************************  "
									+ " ERROR ELIMINANDO CANCIONES DE LOS FAVORITOS "
									+ idUser
									+ " *********************************************.");
							e.printStackTrace();
						}
					}
					session.delete(favFrServN);


				}

			} else {
				logger.info(" ..................................................... "
						+ "---------------------- no hay ninguna lista que eliminar !  ---------------------"
						+ " .....................................................");
			}

			logger.info(" ..................................................... "
					+ " agregando los favoritos subidos al servidor "
					+ " .....................................................");

			if (favoritesFrRequest != null) {

				for (Favorites favoriteFrRN : favoritesFrRequest) {

					// buscamos el usuario de la lista
					session.save(favoriteFrRN);
					List<Canciones> favSongsFromDb = songsDAO
							.getSongsByFavoriteFavoriteIdentification(favoriteFrRN
									.getIdentification());

					logger.info(" songs in favoriteFrR  with name: "
							+ favoriteFrRN.getNameOfList() + "");
					for (Canciones cancionN : favSongsFromDb) {
						logger.info(" > " + cancionN);
						session.save(cancionN);
					}

				}
			} else {
				logger.info(" ..................................................... "
						+ " no se ha subido ninguna lista de favoritos al servidor ! "
						+ " .....................................................");

			}

			transaction.commit();
			// End of transaction
			logger.info("processCommitFavorites() -OK");

			// notificamos al cliente
			final String successJson = new StatusPackage(
					StatusPackage.STATUS_SUCCESS,
					"the favorites list has been updated on the server")
					.serializeToJson();
			out.print(successJson);

		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			out.print(errorJsonObj);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
			favoritesDAO.closeSession(); // _cerramos la sesion_
		}
		return;
	}
 
}
