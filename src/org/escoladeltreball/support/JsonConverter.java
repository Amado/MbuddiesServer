package org.escoladeltreball.support;

import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonConverter {
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
	public static final String
	// Flags
			TAG_SELF = "self",
			TAG_NEW = "new",
			TAG_UDPATE_PROFILE = "update_profile",
			TAG_NEW_FRIENDS_REQ = "newFriendsReq",
			TAG_MESSAGE = "message",
			TAG_EXIT = "exit",
			TAG_NOTIFICATION = "notification",
			// Keys
			KEY_SESSION_ID = "sessionId",
			KEY_TAG = "flag",
			KEY_MESSAGE = "message",
			KEY_TARGET_ID = "targetId",
			KEY_SRC_ID = "srcId",
			KEY_ONLINE_COUNT = "onlineCount",
			KEY_SRC_ID_IMG = "srcIdImg",
			KEY_NUM_FRIENDS_REQ = "numFriendsReq",
			KEY_SRC_NAME = "srcName",
			KEY_TARGET_NAME = "targetName",
			// Notifications.......................
			KEY_TYPE_NOT = "typeNotification",
			NOTIFICATION_NEW_FRIEND_REQUEST = "typeNotNewFriendRequest";

	//
	public static String KEY_NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
	public static String NOT_WARNING = "NOT_WARNING", NOT_ERROR = "NOT_ERROR",
			NOT_INFO = "NOT_ERROR";

	public JsonConverter() {
	}

	/**
	 * Json message to notify to all friends about new contact is connected
	 * */
	public String getNewClientJson(String sessionId, String newContactName,
			String srcUserId, String message, int onlineCount, String idImage) {
		String json = null;

		try {

			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_NEW);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_SRC_NAME, newContactName);
			jObj.put(KEY_SRC_ID, srcUserId);
			jObj.put(KEY_SRC_ID_IMG, idImage);
			jObj.put(KEY_MESSAGE, message);
			jObj.put(KEY_ONLINE_COUNT, onlineCount);

			json = jObj.toString();

			logger.info("JsonConverter - json new client = " + json);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Json message to notify to all friends about new contact is connected
	 * */
	public String getUpdateProfile(String sessionId, String srcName,
			String srcUserId, String message, String idImage) {
		String json = null;

		try {

			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_UDPATE_PROFILE);
			jObj.put(KEY_SRC_NAME, srcName);
			jObj.put(KEY_SRC_ID, srcUserId);
			jObj.put(KEY_SRC_ID_IMG, idImage);
			jObj.put(KEY_MESSAGE, message);
			json = jObj.toString();

			logger.info("JsonConverter - json new client = " + json);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	public String getNotificationMessage(String sessionId, String srcIc,
			String message, String targetId, String notificationType) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_NOTIFICATION);
			jObj.put(KEY_NOTIFICATION_TYPE, notificationType);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_SRC_ID, srcIc);
			jObj.put(KEY_MESSAGE, message);
			jObj.put(KEY_TARGET_ID, targetId);

			json = jObj.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Json message to notify to all friends about new contact is connected
	 * */
	public String getNewFriendRequest(int numRequest, String mesage,
			String idTargetUser) {
		String json = null;

		try {

			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_NEW_FRIENDS_REQ);
			jObj.put(KEY_NUM_FRIENDS_REQ, numRequest);
			jObj.put(KEY_MESSAGE, mesage);
			jObj.put(KEY_TARGET_ID, idTargetUser);
			json = jObj.toString();

			logger.info("JsonConverter - json new client = " + json);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Json message to send to all clients (friends and outisders)
	 * */
	public String getSendAllMessageJson(String sessionId, String fromIdUser,
			String srcUserName, String idTargetUser, String message) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_MESSAGE);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_SRC_ID, fromIdUser);
			jObj.put(KEY_TARGET_ID, idTargetUser);
			jObj.put(KEY_MESSAGE, message);
			jObj.put(KEY_SRC_NAME, srcUserName);
			json = jObj.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * JSON message to send exclusively to one client
	 * */
	public String getSendToTargetMessageJson(String sessionId, String srcId,
			String srcName, String message, String targetId) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_MESSAGE);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_SRC_ID, srcId);
			jObj.put(KEY_SRC_NAME, srcName);
			jObj.put(KEY_MESSAGE, message);
			jObj.put(KEY_TARGET_ID, targetId);

			json = jObj.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * JSON message to send to all friends
	 * */
	public String getSendToAllFriendsMsgJson(String sessionId,
			String fromIdUser, String message) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_MESSAGE);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_SRC_ID, fromIdUser);
			jObj.put(KEY_MESSAGE, message);

			json = jObj.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Json message to send to own client that contains their session details
	 * 
	 * */
	public String getClientDetailsJson(String sessionId, String message) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_SELF);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_MESSAGE, message);

			json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * Json message to notify that a client leaves the connection
	 * */
	public String getClientExitJson(String sessionId, String idUser,
			String message, int onlineCount) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_EXIT);
			jObj.put(KEY_SRC_ID, idUser);
			jObj.put(KEY_SESSION_ID, sessionId);
			jObj.put(KEY_MESSAGE, message);
			jObj.put(KEY_ONLINE_COUNT, onlineCount);

			json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

}
