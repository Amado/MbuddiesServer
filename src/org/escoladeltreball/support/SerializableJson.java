package org.escoladeltreball.support;

import java.util.Arrays;
import java.util.List;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SerializableJson<T extends Object> {

	public String serializeToJson() {
	 
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
				.disableHtmlEscaping().setPrettyPrinting().serializeNulls()
				.create();

		String objectJson = gson.toJson(this);
		return objectJson;
	}
	
	public static <T> String ArrayToString(List<T> list)
	{
	    Gson g = new Gson();

	    return g.toJson(list);
	}

	public static <T> List<T> stringToArray(String s, Class<T[]> clazz) {
		T[] arr = new Gson().fromJson(s, clazz);
		return Arrays.asList(arr);
	}

	@SuppressWarnings("unchecked")
	public SerializableJson<T> deSerializeFromJson(String jsonObject) {
		SerializableJson<T> paquete = null;
		try {

			Gson gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					.disableHtmlEscaping().setPrettyPrinting().serializeNulls()
					.create();

			paquete = gson.fromJson(jsonObject, this.getClass());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paquete;
	}

	public static Class<?> deSerializeFromJson(String jsonObject, Class<?> clase) {
		Class<?> paquete = null;
		try {

			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.setPrettyPrinting().serializeNulls().create();

			paquete = (Class<?>) gson.fromJson(jsonObject, clase);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paquete;
	}

}
