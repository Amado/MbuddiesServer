package org.escoladeltreball.support;

import org.escoladeltreball.model.json.StatusPackage;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class Utils {
 
	public static Class<?> deSerializeFromJson(String jsonObject, Class<?> clase) {
		Class<?> paquete = null;
		try {

			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.setPrettyPrinting().serializeNulls().create();

			paquete = (Class<?>) gson.fromJson(jsonObject, clase);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paquete;
	}
	 

}
