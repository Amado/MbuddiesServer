package org.escoladeltreball.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualTreeBidiMap;
import org.escoladeltreball.dao.FriendRequestDAO;
import org.escoladeltreball.dao.UsersDAO;
import org.escoladeltreball.manager.Manager;
import org.escoladeltreball.model.json.ListFriendRequest.RequestModel;
import org.escoladeltreball.model.persistence.FriendRequest;
import org.escoladeltreball.model.persistence.Users;
import org.escoladeltreball.support.JsonConverter;
import org.hibernate.CacheMode;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.Maps;

@ServerEndpoint("/chat")
public class SocketServlet {
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
	Manager manager;
	// set to store all the live sessions
	private static final Set<Session> sessions = Collections
			.synchronizedSet(new HashSet<Session>());

	/*
	 * usamos un bidimap en lugar de un map porque nos interesa poder buscar por
	 * key y value
	 */
	private static final BidiMap idUserSessionPair = new DualTreeBidiMap();

	private JsonConverter jsonUtils = new JsonConverter();

	// Getting query params
	public static Map<String, String> getQueryMap(String query) {
		Map<String, String> map = Maps.newHashMap();
		if (query != null) {
			String[] params = query.split("&");
			for (String param : params) {
				String[] nameval = param.split("=");
				map.put(nameval[0], nameval[1]);
			}
		}
		return map;
	}

	/**
	 * Called when a socket connection opened
	 * 
	 */
	@SuppressWarnings("unchecked")
	@OnOpen
	public void onOpen(Session openSessionArg) {
		final String openSessionId = openSessionArg.getId();

		logger.info("onOpen() -- the request has ben received  - "
				+ openSessionId + " has opened a connection");
		Map<String, String> queryParams = getQueryMap(openSessionArg
				.getQueryString());

		// caso de error: la peticion no es válida
		if (queryParams.containsKey("idUser") == false) {
			logger.severe("the request doesn't contains any name param !!");
			return;
		}

		String idParam = "";

		// Getting client name via query param
		idParam = queryParams.get("idUser");

		UsersDAO userDao = new UsersDAO();
		userDao.setSession(userDao.getDAOManager());
		Users user = null;
		try {
			logger.info(" idParam = " + idParam);
			user = userDao.getUsersById(Long.parseLong(idParam)).get(0);

		} catch (Exception e) {
			// caso de error:
			logger.info(" The user with id " + idParam + " isn't registered !");
			e.printStackTrace();
			return;
		}

		/**
		 * Atualizamos el estado de conexion del usuario en la base de datos
		 */
		try {
			idParam = URLDecoder.decode(idParam, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		logger.info("**************************** Opening new session for user .... "
				+ idParam + " ************************************************");

		// antes de agregar al usuario a la sesion, lo eliminamos si ya
		// existiera
		removeSessionByIdUser(idParam);
		idUserSessionPair.removeValue(idParam);

		// _Update the user connected stated in database_ and check if the user
		// was registered previously
		boolean connectionSucces = true;
		try {
			this.updateDBConnectedById(Long.parseLong(idParam), true);
		} catch (Exception e) {
			logger.severe("ERROR UPDATING THE USER STATE -  ON OPEN SOCKETSESSION "
					+ e.getMessage());
			connectionSucces = false;
		}

		// caso de error: el usuario no se encuentra en la base de datos
		if (connectionSucces == false) {
			return;
		}

		// caso de exito:

		/**
		 * 
		 * Agregamos la Sesion del Nuevo Usuario en el map de sesiones
		 */
		logger.info("adding the new session to sessions from user " + idParam);
		sessions.add(openSessionArg);

		logger.info("sessions.size = " + sessions.size());
		// añadimos el nombre al map de sessionsNamesPair
		idUserSessionPair.put(openSessionId, idParam);

		logger.info("Searching... for friendsRequest of " + idParam
				+ " with name " + user.getName() + "on database");

		/**
		 * 
		 * Buscamos las peticiones de amistad que ha recibido el usuario
		 * conectado y le enviamos una notificacion con el numero de peticiones
		 */
		try {

			List<FriendRequest> tmpFriendReq = this
					.retrieveFriendRequestFromDB(user.getId());
			if (tmpFriendReq != null && tmpFriendReq.isEmpty() == false) {
				final int numNewRequest = tmpFriendReq.size();
				// ------------------------------------------------------
				// ENVIAMOS UNA NOTIFICACION PARA QUE EL USUARUIO REVISE SUS
				// PETICIONES DE AMISTAD
				// ------------------------------------------------------

				logger.info("--on Open --------- we found " + numNewRequest
						+ " friend request    -----");

				String msgFriendRequest = jsonUtils.getNewFriendRequest(
						numNewRequest, "You have " + numNewRequest
								+ " Friend Request", idParam);

				int i = 0;
				for (FriendRequest friendRequest : tmpFriendReq) {
					i++;
					logger.info(" friendRequest   " + i + ") " + friendRequest);
				}
				try {
					openSessionArg.getBasicRemote().sendText(msgFriendRequest);

					logger.info("the new friends request notification was sent");
				} catch (IOException e) {
					logger.severe("ERROR sending new friends request notification ");
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			logger.severe("ERROR retrieving the friends request from database");
			e.printStackTrace();
		}

		/**
		 * 
		 * Buscamos todos los amigos conectados y les notificamos a cada amigo y
		 * propio usuario por cada amigo conectado
		 */
		logger.info("******************  searching from user friends of:"
				+ user.getName()
				+ " on database ..... ******************************");
		UsersDAO userDAO = new UsersDAO();
		userDAO.setSession(userDAO.getDAOManager());
		List<Users> friends = userDAO.getUsersByFriendId(Long
				.parseLong(idParam));

		final String idImg = user.getIdImg() == null ? null : String
				.valueOf(user.getIdImg());

		final String userName = user.getName();

		logger.info("SocketServerlet - onOpen -> user connected = " + user);
		logger.info("SocketServerlet - onOpen -> user connected (idImg) = "
				+ idImg);
		/**
		 * notificamos al usuario que se acaba de conectar que amigos estan
		 * conectados
		 */

		if (friends != null) {

			logger.info("sending message new Client: " + user.getName()
					+ ", to all friends .........");

			logger.info(friends.size() + " friends Found");
			logger.info("************** showing the friend list from user: "
					+ user.getName()
					+ " and sending newConnection messages for self user");

			for (Users friendN : friends) {
				final Long friendId = friendN.getId();
				final String friendIdStr = String.valueOf(friendN.getId());
				final String friendName = friendN.getName();
				// Notifying all the clients/friends about new person
				if (isConnectedUserId(String.valueOf(friendN.getId()))) {

					// Notifying all the clients/friends about new person joined
					logger.info("sending message new Client: " + user.getName()
							+ ", to all/friends");
					// the session id isn't really needed on newClient message
					final String friendIdImg = friendN.getIdImg() == null ? null
							: String.valueOf(friendN.getIdImg());

					final String userIdImg = user.getIdImg() == null ? null
							: String.valueOf(user.getIdImg());

					final String msgForMySelf = "******** friend already Connected - notify mySelf ******** "
							+ " myUserName = "
							+ userName
							+ ", myId (idParam) = "
							+ idParam
							+ ", friendName "
							+ friendName + ", friendId = ";

					String newFriendClientJsonMsgForSelf = jsonUtils
							.getNewClientJson(openSessionId, friendName,
									friendIdStr, msgForMySelf, sessions.size(),
									friendIdImg);

					try {
						logger.info(" sending notification about friend connection: "
								+ friendId);
						/**
						 * notificamos del amigo conectado
						 */
						logger.info("newClientJsonMsgForSelf ----> "
								+ newFriendClientJsonMsgForSelf);

						boolean done = false;
						int nIntents = 0;
						do {
							try {
								nIntents++;
								openSessionArg.getBasicRemote().sendText(
										newFriendClientJsonMsgForSelf);
								done = true;
							} catch (Exception e) {
								e.printStackTrace();
							}
						} while (done == false && nIntents < 10);

						logger.info("the message was sent");
						try {

							sendMessageToTarget(
									openSessionId,
									idParam,
									userName,
									"******** friend already Connected - notify Friend ******** ",
									true, false, userIdImg, friendIdStr);
						} catch (Exception e) {
							e.printStackTrace();
						}


					} catch (Exception e) {
						e.printStackTrace();
					}
					logger.info(" - " + friendN);

				} else {

					logger.info("----------- The friend " + friendName
							+ " with id: " + friendId
							+ " isn't connected at this moment -----------");
				}
			}

		} else {
			logger.info("NO FRIEND WAS FOUND !");
		}

		/**
		 * enviamos al usuario que se acaba de conectar los detalles de su
		 * conexion
		 */
		try {
			// Sending session id to the client that just connected
			logger.info("Sending session id to the client " + user.getId()
					+ " that just connected");

			openSessionArg.getBasicRemote().sendText(
					jsonUtils.getClientDetailsJson(openSessionId,
							"--- Your session details ---"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("open connection for user " + user.getName()
				+ " successful");
		// logger.info("----- showConnectedUsers----");
		// showConnectedUsers();
	}

	/*
	 * devuelve el id de la session correspondiente al usuario
	 */
	public String removeSessionByIdUser(String idUser) {
		logger.info("removeSessionByIdUser() idUser " + idUser);
		final String sessionId = (String) idUserSessionPair.getKey(idUser);
		Session session = null;
		if (idUser != null) {
			for (Session s : sessions) {
				if (s.getId().equals(sessionId)) {
					session = s;
					break;
				}
				logger.info("session " + s.getId());
			}
		} else {
			logger.info("removeSessionByIdUser() idUser " + idUser
					+ "wasn't found on sessions");
		}

		if (session != null) {
			logger.info("removeSessionByIdUser() idUser " + idUser
					+ "the session of user has been removed");
			sessions.remove(session);
		} else {
			logger.info("removeSessionByIdUser() idUser " + idUser
					+ "couldn't remove the session of user");
		}
		return sessionId;

	}

	public void removeSessionById(String sessionId) {
		Session session = null;

		for (Session s : sessions) {
			if (s.getId().equals(sessionId)) {
				session = s;
				break;
			}
			logger.info("session " + s.getId());

		}

		if (session != null) {
			sessions.remove(session);
		}

	}

	public static boolean isConnectedUserId(String idUser) {
		return idUserSessionPair.getKey(idUser) != null;
	}

	public void showConnectedUsers() {
		logger.info("showConnectedUsers() ");
		// Looping through all the sessions and sending the message individually

		// String idTargetSession = (String) nameSessionPair.getKey(targetName);

		for (Session s : sessions) {
			logger.info("session " + s.getId());
		}
		logger.info("******************************* users connected: *******************************");
		int i = 0;
		for (Object key : idUserSessionPair.keySet()) {
			final String userName = (String) idUserSessionPair.get(key);
			logger.info(" " + (++i) + ") " + userName);
		}
	}

	public List<FriendRequest> retrieveFriendRequestFromDB(Long idUser) {
		UsersDAO usersDAO = new UsersDAO();
		try {
			FriendRequestDAO fRequestDAO = new FriendRequestDAO();
			fRequestDAO.setSession(fRequestDAO.getDAOManager());
			usersDAO.setSession(fRequestDAO.getSession());

			// Users myUser = usersDAO.getUsersByName(myName).get(0);

			// recuperamos solo las peticiones que no se han respondido
			List<FriendRequest> friendRequestList = new ArrayList<FriendRequest>();
			friendRequestList = fRequestDAO.getRequestByUserTarget(idUser,
					RequestModel.REQUEST);

			return friendRequestList;
		} finally {
			usersDAO.closeSession();
		}

	}

	/**
	 * actualiza el estado del usuario en la base de datos a connected throws
	 * exception if the user there isn't registered in database
	 * 
	 * @param name
	 * 
	 */
	public Users updateDBConnectedById(Long idUser, boolean connected)
			throws Exception {
		logger.info("SocketServer -- updateDBConnected() by name");
		Users user = null;
		try {

			UsersDAO usersDAO = new UsersDAO();
			usersDAO.setSession(usersDAO.getDAOManager());
			user = usersDAO.getUsersById(idUser).get(0);
			// Users user = usersDAO.getUsersById(idUser).get(0);
			user.setIsConnected(connected);
			usersDAO.update(user);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new Exception("the user isn't registered in database ", e);
		}
		return user;
	}

	/**
	 * comprueba que la session se encuentre
	 * 
	 * @param sessionId
	 * @return
	 */
	public boolean isInSessions(String sessionId) {
		boolean isIn = false;

		for (Session sessionN : sessions) {
			if (sessionN.getId().equals(sessionId)) {
				isIn = true;
				break;
			}
		}

		return isIn;
	}

	/**
	 * method called when new message received from any client
	 * 
	 * @param message
	 *            JSON message from client
	 */
	@OnMessage
	public void onMessage(final String message, final Session session) {
		logger.info("SocketServlet - onMessage msg");
		logger.info("SocketServlet - onMessage msg Message from "
				+ session.getId() + ": " + message);

		if (isInSessions(session.getId()) == false) {
			logger.warning("An unregistered users couldn't send messages ");
			return;
		}

		String msg = null;
		String targetId = null;
		String srcName = null;
		String tag = null;
		// Parsing the json and getting message
		JSONObject jObj = null;
		try {
			jObj = new JSONObject(message);
			try {
				tag = jObj.getString(JsonConverter.KEY_TAG);

			} catch (Exception e) {
				// nothing to do
				// e.printStackTrace();
			}
			msg = jObj.getString(JsonConverter.KEY_MESSAGE);
			srcName = jObj.getString(JsonConverter.KEY_SRC_NAME);
			targetId = jObj.getString(JsonConverter.KEY_TARGET_ID);

			logger.info("SocketServlet - onMessage msg =  " + msg);

		} catch (Exception e) {
			// --- Do nothing ---
			// e.printStackTrace();
		}
		if (JsonConverter.TAG_UDPATE_PROFILE.equals(tag)) {
			try {

				final String idSrc = jObj.getString(JsonConverter.KEY_SRC_ID);
				final String idImg = jObj
						.getString(JsonConverter.KEY_SRC_ID_IMG);
				/*
				 * final Session sessionArg, String idParam, final String
				 * srcUsername, final String msg, String idImg
				 */
				logger.info("NOTIFYING PROFILE CHANGE FOR CHAT ..........");
				procesNotifyUpdateProfile(session, idSrc, srcName, msg, idImg);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (JsonConverter.TAG_MESSAGE.equals(tag)) {
			// Sends message to target and src
			try {

				processChatMessage(session, srcName, msg, targetId, message);
			} catch (Exception e) {

			}

		} else {

			try {
				/**
				 * --- comprobamos si el mensaje es un RequestModel ---
				 */
				logger.info("SocketServlet - onMessage msg *********************** "
						+ "el mensaje recibido no es un mensaje de chat! - intentando procesar mensaje como una peticion de amistad "
						+ "***************************");
				parseFriendRequest(session, message);
			} catch (Exception ee) {
				logger.severe("********************** NO SE HA PODIDO PROCESAR EL MENSAJE ! -> "
						+ message
						+ " **********************\nPor lo que el mensaje se ha descartado");

				ee.printStackTrace();

			}
		}
	}

	public void procesNotifyUpdateProfile(final Session sessionArg,
			String srcUserId, final String srcUsername, final String msg,
			String idImg) {

		try {

			final String sessionId = sessionArg.getId();
			UsersDAO userDAO = new UsersDAO();
			userDAO.setSession(userDAO.getDAOManager());

			List<Users> friends = userDAO.getUsersByFriendId(Long
					.parseLong(srcUserId));

			/**
			 * notificamos el el cambio a todos los amigos
			 */
			sendMessageToAllFriends(sessionId, srcUserId, srcUsername,
					" notification to all friends  - update profile ", false,
					false, idImg, friends, true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void processChatMessage(final Session session,
			final String srcUsername, final String msg, final String targetId,
			String messageEntire) {
		String myId = (String) idUserSessionPair.get(session.getId());
		logger.info("send message from : id" + session.getId() + " srcName: "
				+ myId + " targetName:" + targetId + " message: " + msg);

		/**
		 * enviamos el mensaje al destinatario y al remitente
		 */

		try {

			sendMessageToTarget(session.getId(), myId, srcUsername, msg, false,
					false, null, targetId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			try {
				JSONObject jObj = new JSONObject(messageEntire);

				jObj.put(JsonConverter.KEY_SRC_NAME, srcUsername);
				messageEntire = jObj.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			logger.info(" message self messageEntire after put srcName and KeyTag = "
					+ messageEntire);
			session.getBasicRemote().sendText(messageEntire);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * intenta procesar el mensaje como un friendRequest, lanza una excepcion
	 * 
	 * @param session
	 * @param message
	 * @return false si el mensaje no contiene un friend request model
	 */
	private boolean parseFriendRequest(Session session, String message)
			throws Exception {
		boolean processOK = false;
		try {

			RequestModel requestFriend = (RequestModel) new RequestModel()
					.deSerializeFromJson(message);


			logger.info("requestFriend  =---->" + requestFriend);
			logger.info("requestFriend.getStatus() :--->  "
					+ requestFriend.getStatus());
			logger.info("requestFriend.getStatus() :--->  "
					+ requestFriend.getIdSrcUser());

			if (requestFriend != null && requestFriend.getIdSrcUser() != null) {
				logger.info("SocketServlet - onMessage msg ---- SocketServlet - OnMessage Procesando peticion de amistad  -----");
				processRequestFriend(requestFriend, session);
			} else {
				throw new Exception();
			}
			processOK = true;
		} catch (Exception ex) {
			logger.info("SocketServlet - onMessage msg *********************** "
					+ "ERROR !! el mensaje " + " ***************************");
			logger.info("message = " + message);
			ex.printStackTrace();
			throw ex;
		}
		return processOK;

	}

	/**
	 * envia un mensaje al cliente destinatario si esta conectado, si no
	 * almacena la peticion en la base de datos del servidor
	 */
	public void processRequestFriend(RequestModel request, Session session) {

		UsersDAO usersDAO = new UsersDAO();
		try {

			String requestStr = request.serializeToJson();
			logger.info("processRequestFriend() requestFriendStr  ="
					+ requestStr);

			// buscamos al destinatario entre los usuarios conectados
			final Long srcId = request.getIdSrcUser();
			final Long trgId = request.getIdTrgUser();

			usersDAO.setSession(usersDAO.getDAOManager());
			/**
			 * ...............................................................
			 */
			usersDAO.getSession().setCacheMode(CacheMode.IGNORE);
			List<Users> friendsFrDB = usersDAO.getUsersByFriendId(srcId);

			boolean areFriends = false;
			logger.info("--------------  comprobando si la peticion se ha realizado a un amigo  ------------------------");
			if (friendsFrDB != null && friendsFrDB.isEmpty() == false) {
				logger.info("--------------  buscando amigos de usuario: "
						+ srcId + "  ------------------------");
				for (Users friendN : friendsFrDB) {
					logger.info(">friendN " + friendN);
					if (friendN.getId().equals(trgId)) {
						areFriends = true;
						break;
					}

				}
			}
			if (areFriends == true) {
				logger.info("--------------  la peticion se ha enviado a un amigo, nada que hacer  ------------------------");
				return;
			}

			logger.info("--------------  se creara un peticion de amistad  ------------------------");

			if (trgId == null || srcId == null) {
				throw new Exception(
						"Error, El id del usuario destinatario o remitente es null -> trgId: "
								+ trgId + ", srcId: " + srcId);
			}

			/**
			 * si la peticion tiene estado Request notificamos al destinatario
			 * de que ha recibido una peticion de amistad nueva
			 */
			boolean requestSendToTarget = false;
			if (RequestModel.REQUEST.equals(RequestModel.REQUEST)) {

				logger.info("trgId  = > " + trgId);
				for (Session sessionN : sessions) {
					String idUserInSessionN = (String) idUserSessionPair
							.get(sessionN.getId());

					if (trgId.equals(Long.parseLong(idUserInSessionN))) {
						try {
							logger.info("--------------  enviando peticion de amistad ...........  ......       . . ."
									+ trgId);
							String msgFriendRequest = jsonUtils
									.getNewFriendRequest(1,
											"You have new Friend Request",
											String.valueOf(trgId));

							sessionN.getBasicRemote()
									.sendText(msgFriendRequest);
							logger.info("se ha enviado la peticion de amistad al usuario, que esta conectado en este momento"
									+ trgId);
							requestSendToTarget = true;
							logger.info("the new friends request notification was sent");
						} catch (IOException e) {
							logger.severe("ERROR sending new friends request notification ");
							e.printStackTrace();
						}
						break;
					}
				}
			}
			if (requestSendToTarget == false) {
				logger.info("processRequestFriend() the user is not connected at this moment "
						+ "\nhe will be notified when he gets connected");
			}
			/**
			 * almacenamos la peticion en la base de datos
			 */
			FriendRequestDAO fRequestDAO = new FriendRequestDAO();
			fRequestDAO.setSession(usersDAO.getSession());

			try {
				List<FriendRequest> requestFromDb = fRequestDAO.getRequests(
						trgId, srcId);
				logger.info(" processRequestFriend() requestFromDb = "
						+ requestFromDb);
				if (requestFromDb != null && requestFromDb.isEmpty() == false) {
					logger.info("********************************************************************************************** "
							+ "\nprocessRequestFriend() friendRequestFromDB that match with current request "
							+ "............  "
							+ "\n**********************************************************************************************");
					for (FriendRequest friendRequest : requestFromDb) {
						logger.info(" processRequestFriend() friendRequest -> "
								+ friendRequest);
					}
					throw new Exception();

				}

			} catch (Exception e) {
				logger.info("********************* ya existia la peticion de usuario (nada que hacer) ********************************\n");

				return;
			}

			/**
			 * almacenamos la peticion en la base de datos
			 */

			Users srcUser = usersDAO.getUsersById(srcId).get(0);
			Users trgUser = usersDAO.getUsersById(trgId).get(0);

			FriendRequest friendRequest = new FriendRequest(trgUser, srcUser,
					RequestModel.REQUEST);

			try {
				fRequestDAO.save(friendRequest);
			} catch (Exception e) {
				e.printStackTrace();
			}

			/**
			 * String srcName, String srcUserId, String message, String idImage
			 */
		} catch (Exception e) {
			logger.severe("********************* Error, al procesar la peticion de amistad"
					+ " revise los datos enviados ********************************\n"
					+ e.getMessage());
		}
	}

	/**
	 * Method called when a connection is closed
	 */
	@OnClose
	public void onClose(Session session) {
		logger.info("Socse()");
		sessions.remove(session);
		logger.info("************************************************************"
				+ "THE SESSION is CLOSED session.id-----> "
				+ session.getId()
				+ "**********************************");
		logger.info("Session " + session.getId() + " has ended");
		// showConnectedUsers();

		// Getting the client name that exited
		String idUser = (String) idUserSessionPair.get(session.getId());
		idUserSessionPair.remove(session.getId());
		// removing the session from sessions list
		// update the connection state into database
		Users user = null;
		try {
			user = this.updateDBConnectedById(Long.parseLong(idUser), false);
		} catch (Exception e) {
			logger.severe("ERROR ON CLOSE SOCKET SESSION " + e.getMessage());
		}

		try {
			this.updateDBConnectedById(Long.parseLong(idUser), true);
		} catch (Exception e) {
			logger.severe("ERROR UPDATING THE USER STATE - ON CLOSE SOCKETSESSION "
					+ e.getMessage());
		}

		// notificamos a todos sus amigos que ha salido
		logger.info("**************************** SocketServlet -onClose the user "
				+ idUser
				+ " with name "
				+ user.getName()
				+ " has left the chat!!! **************************** ");
		sendMessageToAll(session.getId(), idUser, user.getName(),
				String.valueOf(user.getId()), " left the chat", false, true,
				null);

	}
	
	

	/**
	 * Method to send message to all clients
	 * 
	 * @param sessionId
	 * @param message
	 *            message to be sent to clients
	 * @param isNewClient
	 *            flag to identify that message is about new person joined
	 * @param isExit
	 *            flag to identify that a person left the conversation
	 * 
	 */
	private void sendMessageToAll(String sessionId, String idUser,
			String targetName, String targetId, String message,
			boolean isNewClient, boolean isExit, String idImage) {

		logger.info("sendMessageToAll begin ...");
		// Looping through all the sessions and sending the message individually

		logger.info("sessions.size = " + sessions.size());
		for (Session s : sessions) {
			String json = null;

			// Checking if the message is about new client joined
			if (isNewClient) {
				logger.info("new Client");

				logger.info("New client idImage = " + idImage);
				json = jsonUtils.getNewClientJson(sessionId, targetName,
						idUser, message, sessions.size(), idImage);

			} else if (isExit) {
				logger.info("exit Client");
				// Checking if the person left the conversation
				json = jsonUtils.getClientExitJson(sessionId, idUser, message,
						sessions.size());
			} else {
				logger.info("message Client");
				// Normal chat conversation message
				json = jsonUtils.getSendAllMessageJson(sessionId, idUser,
						targetName, targetId, message);
			}

			try {
				logger.info("Sending Message To: " + sessionId + ", " + json);

				s.getBasicRemote().sendText(json);
			} catch (IOException e) {
				logger.severe("ERROR in sending. " + s.getId() + ", "
						+ e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private void sendMessageToAllFriends(String sessionId, String idUser,
			String userName, String message, boolean isNewClient,
			boolean isExit, String idImage, List<Users> friends,
			boolean isUpdateProfile) {

		// search the friends of user
		logger.info("SocketServer -- sendMessageToAllFriends()");
		try {

			if (friends != null) {

				for (Users friendN : friends) {
					// the session id isn't really needed on newClient message
					final Long friendId = friendN.getId();
 
					final String friendNSession = (String) idUserSessionPair
							.getKey(String.valueOf(friendId));

					if (friendNSession != null) {

						for (Session sessionN : sessions) {
							String msgJson = null;

							// Checking if the message is about new client
							// joined
							if (isNewClient) {
								logger.info("new Client .....");
								logger.info("Sending  isNewClient message : "
										+ message);
								logger.info("SocketServer -- sendingMessageToAllFriends() ");
								/**
								 * tring getNewClientJson(String sessionId,
								 * String newContactName, Long idUser, String
								 * message, int onlineCount, String idImage)
								 */
								msgJson = jsonUtils.getNewClientJson(sessionId,
										userName, idUser, message,
										sessions.size(), idImage);

							} else if (isExit) {
								logger.info("exit Client ....");
								// Checking if the person left the conversation
								msgJson = jsonUtils.getClientExitJson(
										sessionId, idUser, message,
										sessions.size());

							} else if (isUpdateProfile) {
								logger.info("update profile ..... ");
								msgJson = jsonUtils.getUpdateProfile(sessionId,
										userName, idUser, message, idImage);

							} else {
								logger.info("message Client");
								msgJson = jsonUtils.getSendToAllFriendsMsgJson(
										sessionId, idUser, message);
							}

							try {
								System.out
										.println("Sending Message To friend: "
												+ friendN.getName()
												+ " from user with id: "
												+ friendId + ", " + msgJson);
								logger.info("Sending message : " + message);
								sessionN.getBasicRemote().sendText(msgJson);
							} catch (IOException e) {
								System.out.println("error in sending. "
										+ sessionN.getId() + ", "
										+ e.getMessage());
								e.printStackTrace();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// private void sendMessageToTarget(String sessionId, String targetName,
	// String message, boolean isNewClient, boolean isExit) {
	private void sendMessagePreFormattedToTarget(String message, String targetId) {

		try {
			logger.info("socketServer -- sendMessagePreFormattedToTarget() message => "
					+ message);
			// Looping through all the sessions and sending the message
			// individually

			String idTargetSession = (String) idUserSessionPair
					.getKey(targetId);
			for (Session s : sessions) {
				try {
					if (s.getId().equals(idTargetSession)) {

						try {
							s.getBasicRemote().sendText(message);
							logger.info("socketServer -- sendMessagePreFormattedToTarget() message => "
									+ message + "was sent !!");
						} catch (IOException e) {
							System.out.println("error in sending. " + s.getId()
									+ ", " + e.getMessage());
							e.printStackTrace();
						}

						break;

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to send message to a cliente
	 * 
	 * @param sessionId
	 * @param msg
	 *            message to be sent to a unique client
	 * @param isNewClient
	 *            flag to identify that message is about new person joined
	 * @param isExit
	 *            flag to identify that a person left the conversation
	 * 
	 */
	private void sendMessageToTarget(String sessionId, String idUser,
			String userName, String msg, boolean isNewClient, boolean isExit,
			String idImage, String targetId) {

		try {

			logger.info("socketServer -- sendMessageToTarget() ");
			// Looping through all the sessions and sending the message
			// individually

			String idTargetSession = (String) idUserSessionPair
					.getKey(targetId);
			for (Session s : sessions) {
				try {
					if (s.getId().equals(idTargetSession)) {

						String msgJson = null;

						// Checking if the message is about new client joined
						if (isNewClient) {
							logger.info("new Client");
							logger.info("Sending  isNewClient message : " + msg);
							logger.info("SocketServer -- sendingMessageToAllFriends() ");
							/**
							 * tring getNewClientJson(String sessionId, String
							 * newContactName, Long idUser, String message, int
							 * onlineCount, String idImage)
							 */
							msgJson = jsonUtils.getNewClientJson(sessionId,
									userName, idUser, msg, sessions.size(),
									idImage);

						} else if (isExit) {
							logger.info("exit Client");
							// Checking if the person left the conversation
							msgJson = jsonUtils.getClientExitJson(sessionId,
									idUser, msg, sessions.size());
						} else {
							logger.info("message Client");
							// Normal chat conversation message
							msgJson = jsonUtils.getSendToTargetMessageJson(
									sessionId, idUser, userName, msg, targetId);

						}

						final String sessionIdN = s.getId();
						final String friendId = (String) idUserSessionPair
								.get(sessionIdN);

						logger.info("sending message to target: " + friendId
								+ " " + "msg: " + msgJson);

						try {
							System.out.println("Sending Message To Self: "
									+ friendId + " from user with id: "
									+ idUser + ", " + msgJson);
							logger.info("Sending message : " + msg);
							s.getBasicRemote().sendText(msgJson);
						} catch (IOException e) {
							System.out.println("error in sending. " + s.getId()
									+ ", " + e.getMessage());
							e.printStackTrace();
						}

						break;
					 
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Session getSession(Long keyIdUser) {
		final String sessionId = (String) idUserSessionPair.get(keyIdUser);

		for (Session s : sessions) {
			if (s.getId().equals(sessionId)) {
				return s;
			}
		}
		return null;
	}

	public static Set<Session> getSessions() {
		return sessions;
	}

	public static BidiMap getIdusersessionpair() {
		return idUserSessionPair;
	}

}