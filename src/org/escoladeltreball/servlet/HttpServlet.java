package org.escoladeltreball.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.escoladeltreball.config.Config;
import org.escoladeltreball.config.Config.HttpActions;
import org.escoladeltreball.dao.CustomDAOSupport;
import org.escoladeltreball.dao.UsersDAO;
import org.escoladeltreball.manager.Manager;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.persistence.Users;
import org.hibernate.Transaction;

/**
 * Servlet implementation class HttpServlet
 */
@WebServlet(name = "HttpServlet", displayName = "HTTP Servlet", urlPatterns = { "/HttpServlet/*" })
public class HttpServlet extends javax.servlet.http.HttpServlet {
	Manager manager = Manager.getIntance();
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

	private static final long serialVersionUID = 1L;

	public HttpServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			// dispath(request, response);
			dispath(request, response);

		} catch (Throwable th) {
			logger.severe("**** ERROR EN EL SERVLET HTTP ***********************************************");
			th.printStackTrace();
		}

	}

	public void dispath(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getRequestURI();

		logger.info("dispath .... ");

		// -- Only for test purpose --
		// (might thrown a ConstraintViolationException: could not execute
		// statement if the exist an user with the same name
		// hibernateTest();

		// extramemos de la URL la accion solicitada
		request.setCharacterEncoding(Config.CHARACTER_ENCODING);
		// establecemos la codificacion para la solicitud
		response.setCharacterEncoding(Config.CHARACTER_ENCODING);
		// establemos la forma en la que enviaremos la respuesta como un objeto
		// json
		response.setContentType("application/json"); //

		String actionRequest = url.substring(url.lastIndexOf("/") + 1,
				url.length());
		// HttpSession session = request.getSession();
		System.out.println("action = " + actionRequest);

		switch (actionRequest) {

		/*
		 * ********************************************************************
		 * > users
		 * ********************************************************************
		 */
		case HttpActions.UPDATE_USER_PROFILE:
			logger.info("dispath -> Update User Profile");
			manager.processUpdateUserProfile(request, response);
			break;

		case HttpActions.FIND_ALL_USERS:
			logger.info("dispath -> Find All Users");
			manager.processFindAllUsers(request, response);
			break;

		case HttpActions.FIND_USER:
			logger.info("dispath -> Find User");
			manager.processFindUser(request, response);
			break;

		case HttpActions.FIND_USERS_BY_COUNTRY_AND_CITY:
			logger.info("dispath -> Find User by City and Country");
			manager.processFindUserByCountryAndCity(request, response);
			break;

		case HttpActions.FIND_DISTINCT_COUNTRIES_AND_CITIES:
			logger.info("dispath -> Find distinct countries and cities");
			manager.processFindDistinctCountriesAndCities(request, response);
			break;

		case HttpActions.FIND_DISTINCT_CITIES_FROM_COUNTRY:
			logger.info("dispath -> Find distinct countries and cities");
			manager.procesFindDistinctCitiesFromCountry(request, response);
			break;

		case HttpActions.SIGNUP:
			logger.info("dispath -> SignUp");
			manager.processSignUp(request, response);
			break;

		case HttpActions.LOGIN:
			logger.info("dispath -> Login");
			manager.processLogin(request, response);
			break;

		case HttpActions.FIND_FRIENDS_REQUEST:
			logger.info("dispath -> Find Friends Request");
			manager.processFindFriendRequest(request, response);
			break;

		case HttpActions.REMOVE_USER:
			logger.info("dispath -> Login");
			manager.processRemoveUser(request, response);
			break;

		// performed isolated to advoid sending the entire user profile every
		// time
		case HttpActions.UPDATE_CURRENT_PLAY_SONG:
			logger.info("dispath -> Update CurrentPlaySong");
			manager.processUpdateCurrentPlaySong(request, response);
			break;

		/*
		 * ********************************************************************
		 * > users and friends
		 * ********************************************************************
		 */
		case HttpActions.ADD_FRIEND:
			logger.info("dispath -> Add Friend");
			manager.processAddFriend(request, response);
			break;

		case HttpActions.DISMIS_FRIEND_REQUEST:
			logger.info("dispath -> Add Friend");
			manager.processDismisFriendRequest(request, response);
			break;

		case HttpActions.FIND_FRIENDS_FROM_USER:
			logger.info("dispath -> Add Friend");
			manager.processFriendsFromUser(request, response);
			break;

		case HttpActions.REMOVE_FRIEND:
			logger.info("dispath -> Remove Friend");
			manager.processRemoveFriend(request, response);
			break;

		case HttpActions.RESPONSE_FRIEND_REQUEST:
			logger.info("dispath -> Response FriendRequest");
			// manager.processResponseFriendRequest(request, response);
			break;

		case HttpActions.SEND_FRIEND_REQUEST:
			logger.info("dispath -> Send FriendRequest");
			// manager.processSendFriendRequest(request, response);
			break;

		/*
		 * ********************************************************************
		 * > favorites / songs
		 * ********************************************************************
		 */
		case HttpActions.FIND_FAVORITES_FROM_USER:
			logger.info("dispath -> Find Favorites From User");
			manager.processFindFavoritesFromUser(request, response);
			break;

		case HttpActions.REMOVE_FAVORITE:
			logger.info("dispath -> Remove Favorite");
			manager.processRemoveFavorite(request, response);
			break;

		case HttpActions.ADD_OR_UPDATE_FAVORITE:
			logger.info("dispath -> Add or Update Favorites");
			manager.processAddOrUpdateFavorite(request, response);
			break;

		case HttpActions.UPDATE_FAVORITE_AND_ITS_SONGS:
			logger.info("dispath -> Update Favorite And Its Songs");
			manager.processUpdateFavoriteAndItsSongs(request, response);
			break;

		case HttpActions.COMMIT_FAVORITES:
			logger.info("dispath -> Commit Favorites ");
			manager.processCommitFavorites(request, response);
			break;

		case HttpActions.FIND_SONGS:
			logger.info("dispath -> Find Songs");
			manager.processFindSongs(request, response);
			break;

		case HttpActions.UPDATE_SONG:
			logger.info("dispath -> Update Song");
			manager.processUpdateSong(request, response);
			break;

		case HttpActions.ADD_SONG_TO_FAVORITE:
			// creates the song if don't exits in DB
			logger.info("dispath -> Add Song to Favorite");
			manager.processAddSongToFavorite(request, response);
			break;

		case HttpActions.REMOVE_SONG_FROM_FAVORITE:
			logger.info("dispath -> Remove Song from Favorite");
			manager.processRemoveSongFromFavorite(request, response);
			break;
		/*
		 * ********************************************************************
		 * > test and mock data
		 * ********************************************************************
		 */
		case HttpActions.CREATE_TEST_DATA:
			logger.info("dispath -> Create Test Data");
			manager.processCreateTestData(request, response);
			break;

		case HttpActions.TEST:
			logger.info("dispath -> Test");
			processTestServlet(request, response);
			break;

		default:
			// mostramos un mensaje de error si no se puede procesar la peticion
			// en formato HTML
			logger.info("### dispath -> ERROR ###");
			// errorConsulta(request, response, actionRequest);
			PrintWriter out = response.getWriter();
			final String errorJsonObj = new StatusPackage(
					StatusPackage.STATUS_ERROR,
					"ERROR ON DISPATCHER COULDN'T PARSE ACTION REQUEST !!!")
					.serializeToJson();
			out.print(errorJsonObj);
			break;
		}

		// manager.cleanFavoritesForUser(21L);
	}

	public void errorConsulta(HttpServletRequest request,
			HttpServletResponse response, String actionRequest)
			throws ServletException, IOException {

		logger.info("testServlet .... ");
		response.setContentType("text/html");
		logger.info("testServlet -> ERROR consulta .... ");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body style='color: red'>");
		out.println("<h1>ERROR EN LA CONSULTA: </h1>");
		out.println("<p>   -> error en la consulta: '" + actionRequest
				+ "'</p>");
		out.println("</body></html>");

		out.close();
	}

	/* 
	 *
	 */
	public void processTestServlet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("testServlet .... ");

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html><body style='color: green'>");
		out.println("<h1>-- testServlet OK --</h1>");
		out.println("</body></html>");

		out.close();
	}

	/**
	 * comprueba si existe modifica el atributo isConnected del usuario y
	 * actualiza sus coordenadas
	 * 
	 * @return null si el usuario no existe
	 */
	public void hibernateTest() {
		logger.info("hibernateTest begin...");
		try {
			Users user1 = new Users("new User in Eclipse", "Joan@gmail.com");
			System.out.println("user 1 = " + user1);

			UsersDAO usersDao = new UsersDAO();
			usersDao.save(user1);

		} catch (Throwable th) {
		} finally {
			logger.info("hibernateTest - finally {}");
		}
	}

	public HashMap<String, Cookie> getCookiesMap(HttpServletRequest request) {
		HashMap<String, Cookie> cookiesMap = new HashMap<String, Cookie>();
		if (request.getCookies() == null) {
			return null;
		}
		for (Cookie cookie : request.getCookies()) {
			cookiesMap.put(cookie.getName(), cookie);
		}

		// System.out.println("COOKIES MAP:");
		for (String key : cookiesMap.keySet()) {
			// System.out.println(cookiesMap.get(key).getName() + " = "+
			// cookiesMap.get(key).getValue());
		}
		return cookiesMap;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// does the same that doGet.
		doGet(request, response);
	}

}
