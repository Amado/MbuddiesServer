package org.escoladeltreball.dao;

import java.util.List;
import java.util.logging.Logger;

import org.escoladeltreball.model.persistence.Users;
import org.hibernate.Transaction;

/**
 * clase que genera un modelo para hacer pruebas
 */
public class MockDAO extends CustomDAOSupport {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

	public List<Users> createTestData() throws Exception {
		List<Users> listUsers = null;

		// CREAMOS LOS USUARIOS
		// Catalunya: Lat: 41,591158900000000000, Long: 1,520862399999941800
		String[] nombres = { "Mirian", "Carlos", "Liliana", "Juan", "Santi",
				"Lucía", "Lucrecia", "Néstor", "Noemí" };
	 

		// double[] longitudes = { 1.5208623999999418, 1.5208623999999418,
		// 1.5208623999999418, 1.5208623999999418, 1.5208623999999418,
		// 1.5208623999999418, 1.5208623999999418, 1.5208623999999418,
		// 1.5208623999999418 };
		//
		// double[] latitudes = { 41.5911589, 41.5911589, 41.5911589,
		// 41.5911589,
		// 41.5911589, 41.5911589, 41.5911589, 41.5911589, 41.5911589 };

		String[] cities = { "Barcelona", "Masnou", "Vilafranca", "Gavà",
				"St. Cugat", "Barcelona", "Barcelona", "Vic", "Barcelona" };

		String[] countries = { "Spain", "Spain", "Spain", "Spain", "Spain",
				"Spain", "Spain", "Spain", "Spain" };

		String[] passwords = { "123456", "123456", "123456", "123456",
				"123456", "123456", "123456", "123456", "123456" };

		Transaction transaction = null;
		// org.hibernate.Session session = null;

		try {
			logger.info("createTestData begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			this.getDAOManager(); // _opens the session_
			transaction = session.getTransaction();
			transaction.begin();

			for (int i = 0; i < nombres.length; i++) {
				// << foto = null & connected = false >>
				/**
				 * Users(String name, String city, String country,
				 * boolean isConnected) {
				 */
 

				Users user = new Users(nombres[i], passwords[i], cities[i],
						countries[i], false);
 

				// creamos el usuario en la base de datos
				session.persist(user);
			}

			session.flush();
			transaction.commit();

		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernateTest()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			// notificamos del error
			throw new Exception("Error al crear los datos de prueba", e);

		} finally {
			// closeSession();
		}
		return listUsers;
	}

}
