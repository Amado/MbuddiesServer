package org.escoladeltreball.dao;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import org.escoladeltreball.model.persistence.Favorites;
import org.hibernate.Query;
import org.hibernate.Transaction;

public class FavoritesDAO extends CustomDAOSupport implements Serializable {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

	public List<Favorites> findAll() {

		Query query = session.createQuery(" select t from Favorites t");
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}

		return null;
	}

	public void save(Favorites arg0) {
		Transaction transaction = null;

		try {
			logger.info("favoritesDAO save() begin... favorites = " + arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.persist(arg0);
			transaction.commit();
			logger.info("favoritesDAO save() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernate()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
		}

	}

	public void saveOrUpdate(Favorites arg0) {
		Transaction transaction = null;

		try {
			logger.info("favoritesDAO save() begin... favorites = " + arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.saveOrUpdate(arg0);
			transaction.commit();
			logger.info("favoritesDAO save() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernate()....");
			e.printStackTrace();
			if (transaction != null) {
				try {
					transaction.rollback();

				} catch (Exception ex) {
					e.printStackTrace();
				}
			}
			throw e;
		} finally {
		}

	}

	public void merge(Favorites arg0) {
		Transaction transaction = null;

		try {
			logger.info("favoritesDAO merge() begin... favorites = " + arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.merge(arg0);
			transaction.commit();
			logger.info("favoritesDAO merge() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernate()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
		}

	}

	public void update(Favorites arg0) {
		Transaction transaction = null;

		try {
			logger.info("favoritesDAO update() begin... favorites = " + arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.update(arg0);
			transaction.commit();
			logger.info("favoritesDAO update() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernate()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
		}

	}

	public void delete(Favorites arg0) {
		Transaction transaction = null;

		try {
			logger.info("favoritesDAO save() begin... favorites = " + arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.delete(arg0);
			transaction.commit();
			logger.info("favoritesDAO save() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernate()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
		}

	}

	public List<Favorites> getFavoritesByIdList(Long idList) {

		Query query = session
				.createQuery(" select t from Favorites t where t.idList = ? ");
		query.setParameter(0, idList);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Favorites> getFavoritesByIdentification(String identification) {

		Query query = session
				.createQuery(" select t from Favorites t where t.identification = ? ");
		query.setParameter(0, identification);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Favorites> getFavoritesByNameOfList(String nameOfList) {

		Query query = session
				.createQuery(" select t from Favorites t where t.nameOfList = ? ");
		query.setParameter(0, nameOfList);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Favorites> getFavoritesByIdentificationAndUserId(
			String identification, Long idUser) {
		Query query = session
				.createQuery(" select t from Favorites t where t.identification like ? AND t.users.id = ?");
		query.setParameter(0, identification);
		query.setParameter(1, idUser);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Favorites> getFavoritesByNameAndIdUser(String nameOfList,
			Long idUser) {
		Query query = session
				.createQuery(" select t from Favorites t where t.nameOfList like ? AND t.users.id = ?");
		query.setParameter(0, nameOfList);
		query.setParameter(1, idUser);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Favorites> getFavoritesByIdUser(Long idUser) {

		Query query = session
				.createQuery(" select t from Favorites t where t.users.id  = ? ");
		query.setParameter(0, idUser);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Favorites> getFavoritesByIdUserWithItsSongs(Long idUser) {

		Query query = session
				.createQuery(" select t from Favorites  t left join fetch  t.canciones  where t.users.id  = ? ");
		query.setParameter(0, idUser);
		List<Favorites> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

}