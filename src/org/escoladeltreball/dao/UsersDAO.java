package org.escoladeltreball.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.escoladeltreball.model.persistence.Users;
import org.hibernate.Query;
import org.hibernate.Transaction;

public class UsersDAO extends CustomDAOSupport implements Serializable {
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

	private static final long serialVersionUID = 1L;

	public List<Users> findAll() {

		try {
			// getDAOManager();
			logger.info("findAll() --- session -> " + session);
			Query query = session
					.createQuery("select t from Users t ORDER BY t.name ASC");
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}

		} finally {
			// closeSession();
		}
		return null;
	}

	public void save(Users arg0) throws Exception {
		Transaction transaction = null;

		try {
			logger.info("userDAO save() begin... user = " + arg0);

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();

			// Session session = null;

			session.persist(arg0);

			transaction.commit();
			logger.info("userDAO save() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernateTest()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
			// closeSession();
		}
	}

	public void delete(Users arg0) throws Exception {
		Transaction transaction = null;

		try {
			logger.info("userDAO delete() begin... user = " + arg0);

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();

			// Session session = null;

			session.delete(arg0);

			transaction.commit();
			logger.info("userDAO delete() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernateTest()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
			// closeSession();
		}
	}

	public void update(Users arg0) throws Exception {
		Transaction transaction = null;

		try {
			logger.info("update User begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.merge(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}
	}

	public List<Users> getUsersByCountryAndCity(String country, String city) {

		try {
			String queryHqlStr = null;

			String param1 = null;
			String param2 = null;

			if (country != null && city != null) {
				queryHqlStr = " select t from Users t where t.country like ? AND "
						+ " t.city like ? ";
				param1 = country;
				param2 = city;
			} else if (country == null && city != null) {
				queryHqlStr = " select t from Users t where t.city like ? ";
				param1 = city;
			} else if (country != null && city == null) {
				queryHqlStr = " select t from Users t where t.country like ? ";
				param1 = country;
			}

			logger.info("getUsersByCityAndCountry( ) queryHQL ->" + queryHqlStr);
			Query query = session.createQuery(queryHqlStr);

			query.setParameter(0, param1);
			if (param2 != null) {
				query.setParameter(1, param2);
			}

			return query.list();
		} finally {
			// closeSession();
		}
	}

	// friends
	public List<Users> getUsersByFriendId(Long friendId) {
		try {

			/*
			 * select distinct client from Client as client inner join
			 * client.groups as grp where grp.extId = :extId
			 */
			// session = getDAOManager();
			Query query = session
					.createQuery(" select distinct u from Users as u inner join u.friends as uf where  uf.id = ?");
			// query.setParameter("id", friendId);
			query.setParameter(0, friendId);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}

		} finally {
			// closeSession();
		}

		return null;
	}

	// // friends
	// public List<Users> getUsersAndItsFriends(Long userId) {
	// try {
	//
	// /*
	// * select distinct client from Client as client inner join
	// * client.groups as grp where grp.extId = :extId
	// */
	// // session = getDAOManager();
	// Query query = session
	// .createQuery(" select distinct u from Users as u inner join u.friends as uf where  u.id = ?");
	// // query.setParameter("id", friendId);
	// query.setParameter(0, userId);
	// @SuppressWarnings("unchecked")
	// List<Users> results = query.list();
	// if (results != null && results.size() > 0) {
	// return results;
	// }
	//
	// } finally {
	// // closeSession();
	// }
	//
	// return null;
	// }

	// friends
	public List<Users> getUsersByFriendName(String friendName) {
		logger.info("getUsersByFriendName " + friendName);
		try {
			Query query = session
					.createQuery(" select distinct u from Users as u inner join u.friends as uf where uf.name like ? ");
			query.setParameter(0, friendName);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			// closeSession();
		}

		return null;
	}

	public List<Users> getUsersByCity(String city) {
		try {
			// session = getDAOManager();
			Query query = session
					.createQuery(" select t from Users t where t.city = ? ");
			query.setParameter(0, city);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}

		} finally {
			// closeSession();
		}

		return null;
	}

	public List<Users> getUsersByCountry(String country) {

		try {
			Query query = session
					.createQuery(" select t from Users t where t.country = ? ");
			query.setParameter(0, country);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}

		} finally {
			// closeSession();
		}
		return null;
	}

	public List<Users> getUsersById(Long id) {
		try {
			logger.info("Searching user by id " + id);
			// session = getDAOManager();
			Query query = session.createQuery("select t from Users t  "
					+ " left join fetch t.canciones where t.id = ? ");
			query.setParameter(0, id);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();

			if (results != null && results.size() > 0) {
				return results;
			}

			// closeSession();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {

		}
		return null;
	}

	public List<Users> getUsersByName(String name) {
		try {
			// Query query = session
			// .createQuery(" select t from Users left join t.canciones c  where c.userses.id = t.id and t.name LIKE ? ");
			Query query = session
					.createQuery(" select t from Users t left join fetch t.canciones  "
							+ "where t.name LIKE ? ");
			query.setParameter(0, name);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<Users> getUsersByIsConnected(Integer isConnected) {

		try {
			Query query = session
					.createQuery(" select t from Users t where t.isConnected = ? ");
			query.setParameter(0, isConnected);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<Users> getUsersByPhoto(String photo) {

		try {

			Query query = session
					.createQuery(" select t from Users t where t.photo = ? ");
			query.setParameter(0, photo);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<String> getDistinctCountries() {
		System.out.println("getCountriesAndCities()");
		Query query = session
				.createSQLQuery("SELECT DISTINCT(country) FROM users order by 1");
		return query.list();
	}

	public List<String> getDistinctCities() {
		Query query = session
				.createSQLQuery("SELECT DISTINCT(city) FROM users order by 1");
		List<String> rows = query.list();
		ArrayList<String> cities = new ArrayList<String>();
		return query.list();

	}

	public List<String> getDistinctCitiesFromCountries(String country) {
		Query query = session
				.createSQLQuery("SELECT DISTINCT(city) FROM users t where t.country LIKE :country order by 1");
		query.setParameter("country", country);
		List<String> rows = query.list();
		ArrayList<String> cities = new ArrayList<String>();
		return query.list();

	}

	public List<Users> getUsersByUser(String user) {

		try {
			Query query = session
					.createQuery(" select t from Users t where t.user = ? ");
			query.setParameter(0, user);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<Users> getUsersByCurrentConnections(Long currentConnections) {

		try {
			Query query = session
					.createQuery(" select t from Users t where t.currentConnections = ? ");
			query.setParameter(0, currentConnections);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<Users> getUsersByTotalConnections(Long totalConnections) {

		try {
			Query query = session
					.createQuery(" select t from Users t where t.totalConnections = ? ");
			query.setParameter(0, totalConnections);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public Users getUserByUsernameAndPassword(String username, String password) {
		@SuppressWarnings("unused")
		Users users = null;
		try {
			Query query = session
					.createQuery(" select t from Users t where t.name = ? AND "
							+ " t.password = ? ");
			query.setParameter(0, username);
			query.setParameter(1, password);
			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results.get(0);
			}
		} finally {
			// closeSession();
		}
		return null;
	}

}
