package org.escoladeltreball.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.escoladeltreball.model.persistence.Canciones;
import org.escoladeltreball.model.persistence.Favorites;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Transaction;

public class SongsDAO extends CustomDAOSupport implements Serializable {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

	public List<Canciones> findAll() {

		Query query = getDAOManager().createQuery(" select t from Canciones t");
		List<Canciones> results = query.list();
		if (results != null && results.size() > 0) {
			return results;
		}
		return null;
	}

	public void save(Canciones arg0) {
		Transaction transaction = null;

		try {
			logger.info("update User begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.persist(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}

	}

	@SuppressWarnings("unchecked")
	/**
	 * List<Object[]> rows = query.list();
	for (Object[] row : rows) {
	Foo foo = new Foo((Long) row[0], (String) row[1], ...);
	}
	 * @param idUser
	 * @return
	 */
	public List<Canciones> getSongsByUser(long idUser) {
		System.out.println("usersDAO getSongsByUser() idUser =  " + idUser);

		String sql = "SELECT DISTINCT(canciones.id_song) as id_song, "
				+ "canciones.song_name as song_name, "
				+ "canciones.song_author as song_author, "
				+ "canciones.src_path as src_path, "
				+ "canciones.photo_encoded as photo_encoded "
				+ "FROM canciones "
				+ "LEFT JOIN songs_and_list ON songs_and_list.id_song = canciones.id_song "
				+ "LEFT JOIN favorites ON favorites.id_list = songs_and_list.id_list "
				+ "WHERE favorites.id_user = :idUser "
				+ "ORDER BY song_name ASC";

		SQLQuery query = session.createSQLQuery(sql);
		// Query query = session.createQuery(sql);
		query.setParameter("idUser", idUser);

		List<Object[]> rows = query.list();
		List<Canciones> songs = new ArrayList<Canciones>();

		for (Object[] row : rows) {
			songs.add(new Canciones(((BigInteger) row[0]).longValue(),
					(String) row[1], (String) row[2], (String) row[3],
					(String) row[4]));
		}
		return songs;
	}

	public List<Canciones> getSongsByFavoriteFavoriteIdentification(
			String favoriteIdentification) {
		// System.out.println("usersDAO getSongsByUser() idUser =  " + idUser);

		String sql = "SELECT DISTINCT(canciones.id_song) as id_song, "
				+ "canciones.song_name as song_name, "
				+ "canciones.song_author as song_author, "
				+ "canciones.src_path as src_path, "
				+ "canciones.photo_encoded as photo_encoded "
				+ "FROM canciones "
				+ "LEFT JOIN songs_and_list ON songs_and_list.id_song = canciones.id_song "
				+ "LEFT JOIN favorites ON favorites.id_list = songs_and_list.id_list "
				+ "WHERE favorites.identification = :favoriteIdentification";

		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("favoriteIdentification", favoriteIdentification);
		List<Object[]> rows = query.list();
		List<Canciones> songs = new ArrayList<Canciones>();

		for (Object[] row : rows) {
			songs.add(new Canciones(((BigInteger) row[0]).longValue(),
					(String) row[1], (String) row[2], (String) row[3],
					(String) row[4]));
		}
		return songs;
	}

	/**
	 * comprueba en cuantas listas de favoritos se encuentra una cancion de un
	 * ususario.
	 * 
	 * @param SongSrcPath
	 * @param idUser
	 * @return
	 */

	public int getFavoritesFromSongAndUser(String SongSrcPath, long idUser) {
		System.out.println("usersDAO getSongsByUser() idUser =  " + idUser);
		/*
		 * identification Columna id_list Columna id_user Columna name_of_list
		 */
		String sql = "SELECT DISTINCT(favorites.id_list) as id_list, "
				+ "favorites.id_user as id_user, "
				+ "favorites.identification as identification, "
				+ "favorites.name_of_list as name_of_list "
				+ "FROM favorites "
				+ "LEFT JOIN songs_and_list ON songs_and_list.id_list = favorites.id_list "
				+ "LEFT JOIN canciones ON canciones.id_song = songs_and_list.id_song "
				+ "WHERE favorites.id_user = :idUser "
				+ "AND canciones.src_path = :srcPath ";

		SQLQuery query = session.createSQLQuery(sql);
		// Query query = session.createQuery(sql);
		query.setParameter("idUser", idUser);
		query.setParameter("srcPath", SongSrcPath);
		List<Object[]> rows = query.list();

		int rowsSize = rows.size();
		logger.info("getFavoritesFromSongAndUser = " + rowsSize);
		// List<Favorites> favorites = new ArrayList<Favorites>();

		/*
		 * Long idList, String nameOfList, String identification, Users users,
		 * Set<Canciones> canciones
		 */
		return rowsSize;

		// return favorites;
	}

	/**
	 * 
	 * @param idUser
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Canciones> getSongBySrcPathAdnUserId(String srcPath, long idUser) {
		System.out.println("usersDAO getSongBySrcPathAdnUserId() idUser =  "
				+ idUser + " srcPath = " + srcPath);

		String sql = "SELECT DISTINCT(canciones.id_song) as id_song, "
				+ "canciones.song_name as song_name, "
				+ "canciones.song_author as song_author, "
				+ "canciones.src_path as src_path, "
				+ "canciones.photo_encoded as photo_encoded "
				+ "FROM canciones "
				+ "LEFT JOIN songs_and_list ON songs_and_list.id_song = canciones.id_song "
				+ "LEFT JOIN favorites ON favorites.id_list = songs_and_list.id_list "
				+ "WHERE favorites.id_user = :idUser AND "
				+ "canciones.src_path like :srcPath "
				+ "ORDER BY song_name ASC ";
		;

		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("idUser", idUser);
		query.setParameter("srcPath", srcPath);

		List<Object[]> rows = query.list();
		List<Canciones> songs = new ArrayList<Canciones>();

		for (Object[] row : rows) {
			/*
			 * Long idSong, String songName, String songAuthor, String srcPath,
			 * String photoEncoded
			 */
			songs.add(new Canciones(((BigInteger) row[0]).longValue(),
					(String) row[1], (String) row[2], (String) row[3],
					(String) row[4]));
		}
		return songs;
	}

	/**
	 * 
	 * @param idUser
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Canciones> getSongBySrcPath(String srcPath) {
		// System.out.println("usersDAO getSongBySrcPathAdnUserId() idUser =  "
		// + " srcPath = " + srcPath);

		String sql = "SELECT DISTINCT(canciones.id_song) as id_song, "
				+ "canciones.song_name as song_name, "
				+ "canciones.song_author as song_author, "
				+ "canciones.src_path as src_path, "
				+ "canciones.photo_encoded as photo_encoded "
				+ "FROM canciones "
				+ "LEFT JOIN songs_and_list ON songs_and_list.id_song = canciones.id_song "
				+ "LEFT JOIN favorites ON favorites.id_list = songs_and_list.id_list "
				+ "WHERE canciones.src_path like :srcPath "

		;

		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("srcPath", srcPath);

		List<Object[]> rows = query.list();
		List<Canciones> songs = new ArrayList<Canciones>();

		for (Object[] row : rows) {
			/*
			 * Long idSong, String songName, String songAuthor, String srcPath,
			 * String photoEncoded
			 */
			songs.add(new Canciones(((BigInteger) row[0]).longValue(),
					(String) row[1], (String) row[2], (String) row[3],
					(String) row[4]));
		}
		return songs;
	}

	public void update(Canciones arg0) {
		Transaction transaction = null;

		try {
			logger.info("update Canciones begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.update(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}
	}

	public void saveOrUpdate(Canciones arg0) {
		Transaction transaction = null;

		try {
			logger.info("update Canciones begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.saveOrUpdate(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}
	}

	public void merge(Canciones arg0) {
		Transaction transaction = null;

		try {
			logger.info("merge Canciones begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.merge(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}
	}

	public void delete(Canciones arg0) {
		Transaction transaction = null;
		try {
			logger.info("delete Canciones begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.delete(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}
	}

	public List<Canciones> getSongsByIdSong(Long idSong) {

		Query query = getDAOManager().createQuery(
				" select t from Canciones t where t.idSong = ? ");

		query.setParameter(0, idSong);

		List<Canciones> results = query.list();

		if (results != null && results.size() > 0) {
			return results;
		}

		return null;
	}

	public List<Canciones> getSongsBySongName(String songName) {

		Query query = getDAOManager().createQuery(
				" select t from Canciones t where t.songName = ? ");

		query.setParameter(0, songName);

		List<Canciones> results = query.list();

		if (results != null && results.size() > 0) {
			return results;
		}

		return null;
	}

	public List<Canciones> getSongsBySongAuthor(Integer songAuthor) {

		Query query = getDAOManager().createQuery(
				" select t from Canciones t where t.songAuthor = ? ");

		query.setParameter(0, songAuthor);

		List<Canciones> results = query.list();

		if (results != null && results.size() > 0) {
			return results;
		}

		return null;
	}

}