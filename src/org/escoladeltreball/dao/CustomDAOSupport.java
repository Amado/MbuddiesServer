package org.escoladeltreball.dao;

import java.io.Serializable;
import java.util.logging.Logger;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class CustomDAOSupport implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(CustomDAOSupport.class
			.getCanonicalName());

	private static final SessionFactory sessionFactory;

	protected Session session;

	static {
		try {

			// creaa un session Factory usando el archivo de configuracion
			// hibernate.cfg.xml
			// situado en la raiz del src
			sessionFactory = new AnnotationConfiguration().configure()
					.buildSessionFactory();
		} catch (Throwable ex) {
			logger.severe("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * @return una session nueva en la base de datos
	 */

	public Session getDAOManager() {
		session = sessionFactory.openSession();
		
		try {
			session.clear();

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("getDAOManager() --- session -> " + session);
		return session;
	}

	/**
	 * cierra la sesion de la base de datos si esta abierta
	 * 
	 * @param session
	 */
	public void closeSession() {
		logger.info("DAO - closing the session...");
		if (session != null && session.isConnected() == true) {
			// cerramos la sesioin para no bloquear el acceso al resto de
			// conexiones
			try {
				if (session.isOpen()) {
					try {
						session.flush();

					} catch (Exception e) {
						e.printStackTrace();
					}
					session.close();

				}
				logger.info("DAO - the session was closed");
			} catch (SessionException e) {
				e.printStackTrace();

			}
		} else {
			logger.info("DAO - the session is alerady close !!");
		}
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

}