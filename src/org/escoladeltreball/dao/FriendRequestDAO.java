package org.escoladeltreball.dao;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import org.escoladeltreball.model.persistence.Canciones;
import org.escoladeltreball.model.persistence.Favorites;
import org.escoladeltreball.model.persistence.FriendRequest;
import org.escoladeltreball.model.persistence.Users;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class FriendRequestDAO extends CustomDAOSupport implements Serializable {
	Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	public List<Users> findAll() {

		try {
			// getDAOManager();
			logger.info("findAll() --- session -> " + session);
			Query query = session.createQuery("select t from FriendRequest t");

			@SuppressWarnings("unchecked")
			List<Users> results = query.list();
			if (results != null && results.size() > 0) {
				return results;
			}

		} finally {
			// closeSession();
		}
		return null;
	}

	/*
	 * id; private Users usersByIdUserTarget; private Users usersByIdUserSrc;
	 * private char status;
	 */

	public void save(FriendRequest arg0) throws Exception {
		Transaction transaction = null;

		try {
			logger.info("friendRequestDAO save() begin... friendRequest = "
					+ arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.persist(arg0);
			transaction.commit();
			logger.info("friendRequestDAO save() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACK Y CERRAMOS LA SESION
			logger.info("ERROR hibernateTest()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
			// closeSession();
		}
	}

	public void update(FriendRequest arg0) throws Exception {
		Transaction transaction = null;

		try {
			logger.info("update FriendRequest begin...");

			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			// this.getDAOManager();
			transaction = session.getTransaction();
			transaction.begin();
			session.merge(arg0);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;

		} finally {
			// closeSession();
		}
	}

	public void delete(FriendRequest arg0) {
		Transaction transaction = null;

		try {
			logger.info("FriendRequestDAO save() delete... FriendRequest = "
					+ arg0);
			// abrimos una sesion e iniciamos una transaccion en la base de
			// datos
			transaction = session.getTransaction();
			transaction.begin();
			// Session session = null;
			session.delete(arg0);
			session.flush();// /////////////////////
			transaction.commit();
			logger.info("favoritesDAO save() commmited ...");
		} catch (Exception e) {
			// SI HAY UN ERROR REALIZAMOS UN ROLLBACk
			logger.info("ERROR hibernate()....");
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
		}

	}
 

	public List<FriendRequest> getRequestByUserTarget(Long idUserTarget,
			String status) {
		logger.info("getRequestByUserTarget()");

		try {

			Query query = session
					.createQuery(" select t from FriendRequest t  "
							+ " WHERE t.usersByIdUserTarget.id = ? AND t.status like ? ");
			query.setParameter(0, idUserTarget);
			query.setParameter(1, status);
			@SuppressWarnings("unchecked")
			List<FriendRequest> results = query.list();

			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<FriendRequest> getRequests(Long idUser1, Long idUser2) {
		logger.info("getRequestByUserTarget()");

		try {

			Query query = session
					.createQuery(" select t from FriendRequest t "
							+ " WHERE ( t.usersByIdUserTarget.id = :user1 AND t.usersByIdUserSrc.id = :user2 ) "
							+ " OR ( t.usersByIdUserTarget.id = :user2 AND t.usersByIdUserSrc.id = :user1 ) ");

			query.setParameter("user1", idUser1);
			query.setParameter("user2", idUser2);
			@SuppressWarnings("unchecked")
			List<FriendRequest> results = query.list();

			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<FriendRequest> getRequestByUserTarget(Long idUserTarget) {
		logger.info("getRequestByUserTarget()");

		try {
			// select a from A a left join fetch a.bs b left join fetch b.cs
			// session = getDAOManager();
			Query query = session
					.createQuery(" select t from FriendRequest t where t.usersByIdUserTarget.id = ? ");
			query.setParameter(0, idUserTarget);
			@SuppressWarnings("unchecked")
			List<FriendRequest> results = query.list();

			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<FriendRequest> getRequestByUserTargetAndSrc(Long idUserTarget,
			Long idUserSource) {
		logger.info("getRequestByUserTarget()");

		try {
			// select a from A a left join fetch a.bs b left join fetch b.cs
			// session = getDAOManager();
			Query query = session
					.createQuery(" select t from FriendRequest t  WHERE "
							+ "t.usersByIdUserTarget.id = :idTrg AND "
							+ "t.usersByIdUserSrc.id = :idSrc");
			query.setParameter("idTrg", idUserTarget);
			query.setParameter("idSrc", idUserSource);
			@SuppressWarnings("unchecked")
			List<FriendRequest> results = query.list();

			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}

	public List<FriendRequest> getRequestByUserTargetOrSource(
			Long idUserTarget, Long idUserSource) {
		try {

			// session = getDAOManager();
			Query query = session
					.createQuery(" select t from FriendRequest t WHERE t.usersByIdUserTarget.id = ? OR t.usersByIdUserSrc.id = ? ");
			query.setParameter(0, idUserTarget);
			query.setParameter(1, idUserSource);
			@SuppressWarnings("unchecked")
			List<FriendRequest> results = query.list();

			if (results != null && results.size() > 0) {
				return results;
			}
		} finally {
			// closeSession();
		}
		return null;
	}
 

}
